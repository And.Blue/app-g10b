<?php
session_start();
/**
 * AUTOLOADER
 * loads classes used automatically
 */
require 'lib/DomHouseFramework/Autoloader.php';
use lib\DomHouseFramework\Autoloader;
use lib\DomHouseFramework\Routing\ControllerFinder;
use lib\DomHouseFramework\Util\RouteHelper;

/**
 * Change this in order to adapt to your environment (or project folder base relative to local server)
 * the routing is through a GET : index.php?controller?method
 */
$dir = (string)str_replace('\\', '/', __DIR__);
const PATH = '/appinfo/';
const ROUTER = 'index.php'; //name used globally for routing

//you must change your directory path change in .htaccess  as RewriteBase f /my_project/
$request = $_SERVER['REQUEST_URI'];
$request = substr_replace($request, '', 0, strlen(PATH) - 1);
$request_uri = explode('?', $_SERVER['REQUEST_URI'], 5);

/**
 * ROUTING SWITCH
 */
try {
    Autoloader::register();

    $pathFinder = new ControllerFinder();

    /**
     * Checks if controller and method are defined from GET (when a Controller redirects to another controller, method
     * redirectToController
     */
    if(isset($_GET['controller']) && $_GET['method'])
    {
        $path = $pathFinder->findGetPath($_GET);
    }
    /**
     * Attempts to link URI to a registered route
     */
    else
    {

        $path = $pathFinder->findRoutePath($request); //finds the path from routes.json
    }
    //in development
    /**
     * *********************************************************************************************************************
     *  Type ?debug in url to get debugging info
     * *********************************************************************************************************************
     */
//
//    $method2 = isset($request_uri[1]) ? $request_uri[1] : '';
//    $parm1 = isset($request_uri[2]) ? $request_uri[2] : 'no parm1 found in uri';
//    $parm2 = isset($request_uri[3]) ? $request_uri[3] : 'no parm2 found in uri';
//    $parm3 = isset($request_uri[4]) ? $request_uri[4] : 'no parm3 found in uri';
//
//    $uriElements[] = $method2;
//    $uriElements[] = $parm1;
//    $uriElements[] = $parm2;
//    $uriElements[] = $parm3;
//    $uriElements[] = $path['method'];
//    RouteHelper::simpleUriDebug($uriElements,$path); //listens for ?debug and displays information regarding routes


    /**
     * *****************************************************************************************************************
     * Gives birth to : instanciates the controller and executes the method.
     * *****************************************************************************************************************
     */
    $pathFinder->birth($path);


} catch (Exception $exception) {
    $exception->getMessage();
}
