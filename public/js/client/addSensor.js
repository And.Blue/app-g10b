const select = document.getElementById('sensor-type');

function ajaxGet(toUrl, callback) {
    let http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            return callback(JSON.parse(this.response));
        } else {
            return {error: 'Ressource not founderror'};
        }
    };
    http.open("GET", toUrl, true);
    http.send();
}

function addOption(select, option) {
    let optionNode = select.appendChild(
        document.createElement('option')
    );
    optionNode.appendChild(
        document.createTextNode(option.text)
    );
    optionNode.value = option.value;
}

ajaxGet('/appinfo/get_sensortypes', (data) => {
    // console.log(data);
    data.map(option => {
            addOption(select, {text: option.name, value: option.id_composant_type});
        }
    );
});
