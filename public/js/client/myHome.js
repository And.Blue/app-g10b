const Spinner = Vue.component('spinner', {
    template: `
    <div class="loader">Loading...</div>
    `
});


const Action = Vue.component('room-action', {
        props: {
            sensor: Object,
        },
        data: function () {
            return ({
                style: {
                    isActive: {color: 'green'},
                    notActive: {color: 'red'}
                },
                message: 'test',
                isActive: null,
                loading: false
            })
        },
        mounted() {
            this.isActive = this.sensor.is_active_component === 1;
        },
        methods: {
            toggleActive: function () {
                this.loading = true;
                this.isActive = !this.isActive;

                axios.post('/appinfo/api/sensors/toggle', {
                    ...this.sensor,
                    is_active_component: this.isActive ? 1 : 0
                })
                    .then(res => {
                            this.loading = false;
                        }
                    )
                    .catch(err => {
                            this.loading = false;
                            console.log(err)
                        }
                    );

            }
        },
        computed: {
            checked: {
                get: function () {
                    return this.isActive;
                },
                set: function (status) {
                    this.isActive = status;
                }
            }
        }
        ,
        template:
            `<div class="component-box">
            <div class="component-box-item">
            <i class="fa fa-power-off sensor-style"></i>
                    {{ sensor.name_component}} 
                    <span v-bind:style="[isActive ? style.isActive:  style.notActive]">
                        <em style="font-size: 0.9rem">  {{ isActive ? 'Activé' : 'Non Activé' }}</em>
                    </span>  
            </div>  
    
             <div>
                  <label class="switch" style="align-self: flex-end">
                <input v-on:click="toggleActive" type="checkbox" v-model="checked" :disabled="loading">
                <span class="slider round"></span> 
                </label>
                <spinner v-if="loading"></spinner>
         
            </div>
            </div>`,
        components: {
            'spinner': Spinner
        }

    },
    )
;



const Sensor = Vue.component('room-sensor', {
    props: {
        sensor: Object
    },
    template:
        `<div class="component-box">
            <div class="component-box-item">
                <i class="fa fa-thermometer-half sensor-style"></i>
                    {{ sensor.name_component}}       
            </div>  
             <div>
             {{ sensor.value_component }}
         
            </div>
            </div>`

});


const Room = Vue.component('room-card', {
    props: {
        room: Object
    },
    template:
        `<div class="card">
                <div class="bg-img">
                    <h2 class="text-img">{{ room.room.name}}</h2>
                </div>
                <div class="content"  v-for="(sensor, index) in room.sensors">
                    <div v-if="sensor.is_active_component !== null">
                        <room-action v-bind:sensor="sensor"></room-action>                    
                    </div>
                    <div v-else>
                        <room-sensor v-bind:sensor="sensor"></room-sensor>
                    </div>
                 </div>
            </div>`,
    components: {
        'room-action': Action,
        'room-sensor': Sensor
    }
});

