var btnRegister = document.getElementById("btn-register");
var btnLogin = document.getElementById("btn-login");
var res = document.getElementById('registerStatus');
var resLogin = document.getElementById('loginStatus');
var inputName = document.getElementById('name');
var inputSurname = document.getElementById('surname');
var inputLoginSurname = document.getElementById('surnameLogin');

btnRegister.addEventListener('click', userAdd);
btnLogin.addEventListener('click', userlogin);

function userAdd() {
    return ajaxPost('/appinfo/modal_register_user', 'name=' + inputName.value + '&surname=' + inputSurname.value, handleData);
}

function userlogin() {
    // resLogin.innerText = inputLoginSurname.value;
    return ajaxPost('/appinfo/modal_login_user', 'surname=' + inputLoginSurname.value, handleLogin);
}

function handleLogin(data) {
    console.log(data.status);
    if(data.status === 'success'){
        resLogin.innerHTML = '<div class="isa_success"><i class="fa fa-check"></i>' +
            'Status : ' + data.status + ' !</div>';
        console.log('reached true');

    } else {
        resLogin.innerHTML = '<div class="isa_error"><i class="fa fa-times-circle"></i>'+
            'Something went wrong...</div>';
    }
}

function handleData(data) {
    inputName.value = '';
    inputSurname.value = '';

    res.innerText = data.status;
    if (data.status === 'success') {
        res.innerHTML = '<div class="isa_success"><i class="fa fa-check"></i>' +
            'Successfully added ' + data.name + ' ' + data.surname + '!</div>';

    } else {
        res.innerHTML = '<div class="isa_error"><i class="fa fa-times-circle"></i>' +
            'Something went wrong...</div>';
    }

}

function ajaxPost(toUrl, param, callback) {
    var data; //response data
    var http = new XMLHttpRequest();
    var url = toUrl;
    var params = param;
    http.open('POST', url, true);

    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function () {//Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {
            if (typeof JSON.parse(http.response) !== 'undefined') {
                data = JSON.parse(http.response);
            } else {
                data = {status: 'undefined data from backend'}
            }
            callback(data);
        } else {
            data = {status: 'http error'};
            callback(data);
        }
    };
    http.send(params);
    console.log('ready state : '+ http.readyState + ' status : '+ http.status);
}

