/**
 * This script
 */
console.log('loaded navbarModals.js');

/**
 * Modals for Login and Register (pop-ups)
 * @type {HTMLElement}
 */
var triggerLogin = document.getElementById("trigger-login");
var triggerRegister = document.getElementById("trigger-register");
var modalLogin = document.getElementById("modal-login");
var modalRegister = document.getElementById("modal-register");
var closeButtonLogin = document.getElementById("close-button-login");
var closeButtonRegister = document.getElementById("close-button-register");
var btnRegister = document.getElementById("btn-register");

function toggleModalLogin() {
    modalLogin.classList.toggle("show-modal");
}

function toggleModalRegister() {
    modalRegister.classList.toggle("show-modal");
}



function windowOnClick(event) {
    if (event.target === modalLogin) {
        toggleModalLogin();
    } else if (event.target === modalRegister) {
        if(event.target === btnRegister) {
            event.preventDefault();
        }
        toggleModalRegister();
    }
}

triggerLogin.addEventListener("click", toggleModalLogin);
closeButtonLogin.addEventListener("click", toggleModalLogin);
closeButtonRegister.addEventListener("click", toggleModalRegister);
triggerRegister.addEventListener("click", toggleModalRegister);
window.addEventListener("click", windowOnClick);
