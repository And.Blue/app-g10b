
const newMessage =
    '<div id="new-box">\n' +
    '\n' +
    '    <div class="card-container">\n' +
    '        <div class="card-big u-clearfix">\n' +
    '            <div class="card-head">\n' +
    '                <span class="right-side" onclick="sendForm()"><i\n' +
    '                            class="fa fa-paper-plane" style="cursor: pointer"></i></span>\n' +
    '                <span class="left-side">Nouveau message</span>\n' +
    '            </div>\n' +
    '            <div class="card-body">\n' +
    '                <span class="card-description"></span>\n' +
    '                <form action="" method="post" id="new-message">\n' +
    '                    <textarea name="new-message" id="" cols="15" rows="5"></textarea>\n' +
    '                </form>\n' +
    '                <div class="card-read"></div>\n' +
    '                <!--                <span class="card-tag card-circle subtle">C</span>-->\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <div class="card-shadow"></div>\n' +
    '    </div>\n' +
    '</div>';


const lastMessage = document.getElementById('last-message');


let messageDrawerStatus = false;

const sendForm = () => {
    document.getElementById('new-message').submit();

};


const messageDrawer = (elem) => {
    if(!messageDrawerStatus) {
        messageDrawerStatus = !messageDrawerStatus;
        return elem.insertAdjacentHTML('afterend', newMessage);
    }
    messageDrawerStatus = !messageDrawerStatus;
    let box = document.getElementById('new-box');
    box.parentNode.removeChild(box);
};


function applySearch() {
    var input, filter, cardList, cards, title, i, txtValue;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();
    cardList = document.getElementById("dynamicCard");
    cards = cardList.getElementsByClassName("card-container");
    for (i = 0; i < cards.length; i++) {
        title = cards[i].getElementsByClassName("card-title")[0];
        console.log('title', title.textContent);
        txtValue = title.textContent || title.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            cards[i].style.display = "";
        } else {
            cards[i].style.display = "none";
        }
    }
}
