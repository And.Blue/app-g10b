/**
 * Search Engine Algorithm
 * @param users
 * @param input
 */
const searchEngine = (users, input) => {
        if (!Array.isArray(users)) {
            return console.log('Input is not an Array');
        }

        let searchResult = [];

        users.map(user => {
            if (user.name.substring(0, input.length) === input) {
                console.log(`user: ${user.name.substring(0, input.length)} input: ${input}`);
                searchResult.push(user);
            }
        });

        if (searchResult.length < 1) {
            console.log('No search result found');
        } else {
            console.log('res found: ', searchResult);
        }
        return searchResult;
    }
;