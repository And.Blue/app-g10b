<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/1/2019
 * Time: 6:24 PM
 */
namespace lib\DomFramework\Controller;

class ErrorController extends AbstractController
{
    public function index()
    {
        $templateData['error']= 'error from Error Controller';
        return $this->render('templates/errors/routeDebug.php',$templateData);
    }

    public function routing()
    {
        $templateData['error']= 'Routing from Error Controller';
        return $this->render('templates/errors/routeDebug.php',$templateData);
    }

}