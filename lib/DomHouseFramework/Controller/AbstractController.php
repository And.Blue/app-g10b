<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/24/2019
 * Time: 8:33 PM
 */
namespace lib\DomHouseFramework\Controller;

use lib\DomHouseFramework\Routing\ControllerRedirector;
use lib\DomHouseFramework\View\Templator;


abstract class AbstractController
{
    /**
     * @var Templator
     */
    private $templator;
    private $redirector;
    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $this->templator = new Templator();
        $this->redirector = new ControllerRedirector();
    }

    /**
     * First Part of the templating engine
     * Uses the templator in the controller to render template
     * @param string $template
     * @param null $data
     * @param null $message
     * @return bool
     */
    protected function render(string $template, $data = null,$message = null)
    {
        return $this->templator->assemble($template,$data,$message);
    }

    /**
     * @param string $address
     * @return bool
     */
    protected function redirectToHTTP(string $address)
    {
        return $this->redirector->toHTTP($address);
    }

    /**
     * @param string $route
     * @return bool
     */
    protected function redirectToRoute(string $route, $message = null)
    {
        return $this->redirector->toRoute($route, $message);
    }

    /**
     * @param string $controllerPath
     * @param string $controllerMethod
     * @return bool
     * @param null|array $parameters
     */
    protected function redirectToController(string $controllerPath, string $controllerMethod, $parameters = null)
    {
        return $this->redirector->toController($controllerPath,$controllerMethod,$parameters);

    }

    protected function checkRole(string $role) {

        if (!(isset($_SESSION['role']) && $_SESSION['role'] === $role)) //checks session if user is admin
        {
            return $this->redirectToRoute("/illegal-credentials");
        }

    }
}