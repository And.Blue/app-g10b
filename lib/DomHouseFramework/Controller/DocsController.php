<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/8/2019
 * Time: 10:59 AM
 */

namespace lib\DomHouseFramework\Controller;

/**
 * Class DocsController
 * The purpose here is to explain how to use things.
 * @package lib\DomFramework\Controller
 */
class DocsController extends AbstractController
{
    public function home()
    {
        return $this->render('lib/DomHouseFramework/templates/documentation/home.php');
    }

    public function page1()
    {
        return $this->render('lib/DomHouseFramework/templates/documentation/controllers.php');
    }

    public function page2()
    {
        return $this->render('lib/DomHouseFramework/templates/documentation/templates.php');
    }

}