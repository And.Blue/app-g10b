<table>
    <tr style="color: white;background: black">
        <th>Field Key</th>
        <th>Field Value</th>
        <th>Not Filled</th>
    </tr>

    <?php
    foreach ($this->fields as $field => $value) {
        $value = isset($field) ? $value : 'no filed';
        $mustFill = $value[1] ? 'true' : 'false';
        echo '<tr>
            <td>' .$field. '</td>
            <td>' . $value[0] . '</td>
            <td>'.$mustFill . '</td>
            </tr>';
    }
    ?>
</table>

