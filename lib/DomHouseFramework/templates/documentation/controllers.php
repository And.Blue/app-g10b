<?php $title = 'Controllers'; ?>
<?php require 'lib/DomHouseFramework/templates/documentation/doc-navbar.php';?>

<div class="header">
    <h1>Controllers</h1>
</div>

<p>Le contrôleur dans une architecture MVC est essentiel car il va se charger de faire le lien entre le Modèle (fonction
    et classes permettant de faire des requêtes à la base de données, gestion des entités etc.) et la vue, ce que l’on
    voit (html, css, js) côté utilisateur. Tout passe par le contrôleur ! </p>

<p>Dans ce Framework, un contrôleur est un objet (classe instanciée) qui hérite d’une classe mère AbstractController,
    contenant des services propres au contrôleurs (ex : l’objet Templator qui permet de créer une vue, ou l’objet
    ControllerRedirector qui permet d’effectuer des redirections).</p>

<p><em>Ci-dessous se trouve un exemple de Controller. Il est important d'essayer de nommer ses contrôleurs de la forme XXXController
    et de le placer dans src/Controller, par soucis de clareté et structure.
    </em></p>

<?prettify?>
<pre class="prettyprint">
    namespace src\Controller;

    use lib\src\Controller\AbstractController;

    class MyController extends AbstractController
    {
    public function index() //methode du Controller qui sera executée
    {
    /*
     * $contenu défini les données et contenus généré par le modèle (ex:  un message ou une array d'utilisateurs) à passer à la vue.
     */
    $contenu = 'bonjour';
    /*
     * Génère la vue avec le template choisi, en lui passant du contenu à afficher
     */
    return $this->render('templates/pages/nom_de_mon_template.php', $contenu);
    }
</pre>