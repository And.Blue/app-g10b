<?php $title = 'Templates'; ?>

<?php require 'lib/DomHouseFramework/templates/documentation/doc-navbar.php'; ?>

<div class="header">
    <h1>Templates</h1>
</div>

<p>Les templates sont les pages HTML avec du CSS qui composent la Vue du modèle MVC. Ils héritent tous de base du
    template de base, soit le fichier avec la mise en forme et le css général.
</p>

<p>Le css des templates templates doit (par soucis de clareté) être contenu dans le dossier public à la racine du
    projet.</p>


<p><em>Pour générer une vue, la méthode render (appartenant à l'objet Templator hérité par AbstractController), prend
        comme dans le cas ci-dessous
        en argument le chemin du template choisi et les variables (contenues dans un array s'il y a plusieurs variables)
        à passer à la vue. La méthode render dans les grandes lignes</em></p>
<? prettify ?>
<pre class="prettyprint">
    namespace src\Controller;

    use lib\src\Controller\AbstractController;

    class MyController extends AbstractController
    {
    public function index() //methode du Controller qui sera executée
    {
    /*
     * $contenu défini les données et contenus généré par le modèle (ex:  un message ou une array d'utilisateurs) à passer à la vue.
     */
    $contenu['mot_du_jour'] = 'bonjour';
    /*
     * Génère la vue avec le template choisi, en lui passant du contenu à afficher
     */
    return $this->render('templates/pages/nom_de_mon_template.php', $contenu);
    }
</pre>

<p>Les templates ont la forme ci dessous : </p>
<p>Le nom des variables dans le controleur (au dessus) n'ont pas 'importance <br>
    par contre il faut toujours utiliser <em>$templateData</em> pour les données qui sont passées par le templates. Dans ce cas un <br>
    array dictionnaire (recommandé) a été passé dans render(). Le template accède donc au contenu par la clé définie dans le controleur.
</p>
<pre class="prettyprint">
&#60?php $title = 'Home'; ?&#62 //ceci permet de modifier le titre de la page
&#60h1&#62Home&#60/h1&#62
    &#60p&#62&#60?=$templateData['mot_du_jour']?&#62&#60/p&#62
&#60p&#62Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquid aut culpa, cumque dolor, dolore est <br>
    exercitationem in ipsam laboriosam laborum maiores minima placeat possimus quaerat unde ut velit voluptate!&#60/p&#62


</pre>