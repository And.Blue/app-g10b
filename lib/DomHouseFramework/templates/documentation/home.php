<?php $title = 'Documentation Index'; ?>


<div class="header">
    <h1>Documentation DomHouse Framework</h1>
</div>
<p>DomHouseFramework est un léger Framework PHP développé pour le projet d'APP. C'est un framework MVC en Orienté
    Objet.</p>
<p>Le but de ce Framework est d'avoir un code plus propre, plus compréhensible, facile d'utilisation, avec réécriture
    d'URL, tout en gardant une structure solide dans le code. </p>

<p>La Documentation se découpe en plusieurs parties afin de clarifié les fonctionnalités les plus importantes du Framework qui sont fonctionnelles.</p>
<h2>Pages de documentation :</h2>
<ul>

    <li></i><a href="<?=PATH?>docs/1">Controllers</a></li>
    <li><a href="<?=PATH?>docs/2">Créer une Page (Template)</a></li>
    <li><i class="fa fa-road fa-2x" aria-hidden="true"></i>Routing : différents Types de Router</li>
    <li>Configuration</li>

</ul>