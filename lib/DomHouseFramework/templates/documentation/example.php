<?php $title = 'Example'; ?>

<div class="header">
    <h1>Hello, this is where it starts ! </h1>
</div>
<h2>#1 This is an MVC Framework (for a school project)</h2>
<p>Model–View–Controller (usually known as MVC or Model WC for the MVVC variant) is an architectural pattern commonly
    used for developing user interfaces that divides an application into three interconnected parts. This is done to
    separate internal representations of information from the ways information is presented to and accepted from the
    userThe MVC design pattern decouples these major components allowing for efficient code reuse and parallel
    development...</p>
<span><em>source <a
                href="https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller">this wikipedia article</a> </em></span>
<p>Here the purpose is to have a clean code using Object Oriented Programming, with an MVC structure (to get a better
    grade ;) ) and something simple to debug and use when working as a group ! </p>


<h2>#2 How to use it ? </h2>
<p>The Framework's source code is essentially within lib/DomeFrameWork. Every Controller you create should be within
    src/Controller and be nammed XXXController so it's and extends AbstractController (in order to have all the services you need to generate your <em>templates</em> (plain html files with a bit of php in it).
    It's important to specify the namespace in which you /src classes are. AbstractController enables your Controller to link View and Model with No problem.
</p>
<p>
    
</p>