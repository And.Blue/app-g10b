<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/24/2019
 * Time: 9:16 PM
 */

namespace lib\DomHouseFramework\View;

class Templator
{
    /**
     * Takes in as parameter the template used (no ob_start and end) with the variable used
     * @param $template
     * @param $data
     * @return bool
     */
    public function assemble($template, $templateData, $message=null)
    {

        ob_start();
        require $template;
        $content = ob_get_clean();
        require 'templates/base.php';
        return true;
    }

}