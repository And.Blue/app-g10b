<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/12/2019
 * Time: 9:35 AM
 */

namespace lib\DomHouseFramework\Forms;

use lib\DomHouseFramework\View\Templator;

/**
 * This class is Makes object oriented FORMS with a dynamic POST validation
 * the strategy is adding fields to the Form, fields with names identical to the ones in the template.
 * Class Form
 * @package FrameworkForms
 */
class Form
{
    protected $formTemplate;
    protected $fields;
    protected $templator;
    protected $submittedOnce = false;
    protected const POST_INFO = 'lib/DomHouseFramework/templates/forms/postInfo.php';
    protected const FIELDS_INFO = 'lib/DomHouseFramework/templates/forms/formDebug.php';

    /**
     * Form constructor.
     * @param string $formTemplate
     */
    public function __construct(?string $formTemplate)
    {
        $this->formTemplate = $formTemplate;
        $this->fields = [];
        $this->templator = new Templator();
    }

    /**
     * to add usable fields to the Form
     * @param string|array $field
     * @param bool $hasToBeFiled
     */
    public function add($field, $hasToBeFiled = true)
    {
        if(!is_array($field))
        {
            $this->fields[$field] = [null, $hasToBeFiled]; //value is an array
        } else
        {
            foreach ($field as $value)
            {
                $this->fields[$value] = [null, $hasToBeFiled];
            }
        }
    }

    /**
     * Compares POST and Form fields
     */
    public function showFields()
    {
        require '' . self::FIELDS_INFO . '';
    }

    public function showPosts()
    {
        $postArr = $_POST;
        require '' . self::POST_INFO . '';
    }


    /**
     * Builds the view of the Form
     * @param null $templateData
     */
    public function buildView($templateData = null)
    {
        $this->postSubmitter(); //when Form handles the POST of the encapsulated template

        $this->templator->assemble($this->formTemplate,$templateData);
//        require '' . $this->formTemplate . '';
    }

    /**
     * Compares POST and field of the FORM
     * adds the values of POST to field.
     * keys of POST should be the same as the ones from FORM
     */
    protected function postSubmitter()
    {
        if (sizeof($_POST) != 0) {
            $postArr = $_POST;
            $this->submittedOnce  = true;

            foreach ($this->fields as $field => $value) {
                if (isset($postArr[$field]) && (key_exists($field, $postArr))) {
                    $this->fields[$field][0] = $postArr[$field];
                    $this->fields[$field][1] = false;

                }
            }
        }
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return mixed
     */
    public function getSubmittedOnce()
    {
        return $this->submittedOnce;
    }


    /**
     * Verifies that every field of Form has been completed (has met POSTed front form)
     * @return bool
     */
    public function isComplete(): bool
    {
        foreach ($this->fields as $field)
        {
            if($field[1] || $field[0] == '') //not complete when form value of to be completed is still true
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Converts the fields to a More Exploitable Data array
     * @return array
     */
    public function getData() : array
    {
        $data = [];
        foreach ($this->fields as $key => $field)
        {
            $data[$key]  = $field[0];
        }
        return $data;
    }
}