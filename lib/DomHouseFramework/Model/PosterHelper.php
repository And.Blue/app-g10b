<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/20/2019
 * Time: 4:52 PM
 */

namespace lib\DomHouseFramework\Model;


/**
 * Class PosterHelper
 * This Class helps handling with posts and have some abstraction
 * @package lib\DomHouseFramework\Model
 */
class PosterHelper
{
    protected $postData;
    protected $expectedValues;
    /**
     * PosterHelper constructor.
     * @param $postData
     */
    public function __construct()
    {
        $this->postData = $_POST;
        $this->expectedValues = [];
    }


    /**
     * This adds values that are expected to be posted
     * @param $expectedPost
     */
    public function add($expectedPost){
        $this->expectedValues[] = $expectedPost;
    }

    /**
     * Debug of attributes (_POST and Expected to be Posted)
     */
    public function debug()
    {
        echo '_POST';
        var_dump($this->postData);
        echo 'Expected';
        var_dump($this->expectedValues);
        $this->isComplete();
    }

    /**
     * @return array
     */
    public function getPost() : array
    {
        return $this->postData;
    }

    /**
     * Checks if Expected to Be posted values Are Posted
     * @return boolean
     */
    public function isComplete() : bool
    {
        $verifArr = [];
        foreach ($this->expectedValues as $expectedValue)
        {
            foreach ($this->postData as $post => $postDatum)
            {

                if($post == $expectedValue && $postDatum !== '')
                {
                    $verifArr[] = $expectedValue;
                }
            }
        }
        $verifArr = array_unique($verifArr); //just in case there are duplicates
        return (sizeof($verifArr) == sizeof($this->expectedValues));
    }
}