<?php

namespace lib\DomHouseFramework\Model;

class Response
{
    protected $headers;
    protected $payload; //contains the data that should be passed to client
    protected $statusCode;
    protected $allStatusCodes = array(
      '200' => '200 OK!',
      '400' => '400 Bad Request',
      '401' => '401 Unauthorized, Session Role needs to be activate and/or of higher authorization level',
      '422' => 'Unprocessable Entity',
      '500' => '500 Internal Server Error'
    );

    /**
     * Response constructor.
     * @param int $statusCode standard response code
     * @param $payload data to pass to client
     */
    public function __construct($statusCode = 200, $payload = null)
    {
        header_remove();
        http_response_code($statusCode); //set Response code
        $this->statusCode = $statusCode;
        $this->headers = getallheaders(); //stores headers
        $this->payload = $payload;

    }

    public function sendJson() : string
    {
        header('Content-Type: application/json');
        header('Cache-Control: no-transform, public, max-age=300, s-maxage=900');

        //ok status code
        if(array_key_exists($this->statusCode,$this->allStatusCodes))
        {
            header('Status: '.$this->allStatusCodes[$this->statusCode]);
            return json_encode(array(
                'status' => $this->allStatusCodes[$this->statusCode],
                'data' => $this->payload
            ));

        }
        //status code not supported
        header('Status: 500 status code not found or not supported');
        return json_encode(array(
           'status' => '500 status code not found or not supported',
           'message' => 'status code is not supported on this server'
        ));

    }
}