<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/21/2019
 * Time: 6:45 PM
 */
namespace lib\DomHouseFramework\Model;


abstract class AbstractRepository
{
    protected $database; //databased directly loaded at construction

    /**
     *  AbstractRepository constructor.
     */
    public function __construct()
    {
        $this->database = DatabaseConnect::getDb();
    }

}