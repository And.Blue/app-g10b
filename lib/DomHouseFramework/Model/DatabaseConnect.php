<?php

namespace lib\DomHouseFramework\Model;
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/21/2019
 * Time: 12:40 PM
 */

use PDO;
use lib\DomHouseFramework\Util\JsonHelper;

class DatabaseConnect
{
    private static $credentialsFile = 'config/db_credentials.json';

    public static function getDb(): PDO
    {
        $credentials = self::getCredentials();

        $db = new PDO('' . $credentials['host'] . ';dbname=' . $credentials['db_name'] . ';charset=' . $credentials['charset'],
            $credentials['username'], $credentials['password']);
//        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $db;
    }


    /**
     * @return array
     */
    private static function getCredentials(): array
    {
        return JsonHelper::getArrFrom(static::$credentialsFile);
    }


}