<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/8/2019
 * Time: 11:35 AM
 */

namespace lib\DomHouseFramework\Routing;

use lib\DomHouseFramework\Util\JsonHelper;

class ControllerFinder
{
    //error pages
    const ERROR_404 = 'templates\errors\404.php';
    const ROUTE_ERROR = 'lib\DomHouseFramework\Model\ErrorController.php';

    //file containing routes
    const ROUTES = "config/routes.json";

    protected $routes;
    protected $requestedRoute = null;
    protected $routeAction;
    protected $method = null; //method that RouteAction will be executing var not used atm
    protected $controllerPath; //controller's path in src var not used atm
    protected $path = null;
    protected $param = null;

    /**
     * @param $path
     * @return string|void
     */
    public function birth($path)
    {
        if ($this->requestedRoute == null) {
            $path['path'] = $this->controllerPath;
            $path['method'] = $this->method;
            $path['param'] = $this->param; //passes simple parameters
            return $this->routeAction->generate($path);
            //            return require ''.(string)$path.'';
        } else {
            $path['path'] = self::ROUTE_ERROR;

            return $this->routeAction->generate($path);
        }
    }

    /**
     * Checks in the registered paths file (routes.json) is the route is matched
     * @param $request
     * @return mixed|array
     */
    public function findRoutePath($request): array
    {
        $request = explode('?', $request);
        $route = $request[0];
        $param = isset($request[1]) ? $request[1] : null; //sets the params to Controller finder
        $this->requestedRoute = $this->cleanRoute($route);
        if (key_exists($route, $this->routes)) {
            $this->setControllerPath($this->routes[$route][0]);
            $this->setMethod($this->routes[$route][1]);
            $this->setParam($param);
            return $this->path;
        } else {
            header('HTTP/1.0 404 Not Found');
            $this->setControllerPath(self::ERROR_404);
            return $this->path;
        }
    }

    /**
     * @param $request
     * @return mixed
     */
    public function findGetPath($request)
    {
        $path['controller'] = $request['controller'];
        $path['path'] = $request['controller'];
        $path['method'] = $request['method'];

        return $path;
    }


    /**
     * ControllerFinder constructor.
     */
    public function __construct()
    {
        $registeredRoutes = JsonHelper::getArrFrom(self::ROUTES);
        $this->routes = $registeredRoutes;
        $this->routeAction = new RouteAction();

    }


    /**
     * Gets only the Controller name from the uri
     * @param $routeToTrim
     * @return bool|string
     */
    private function cleanRoute($routeToTrim)
    {
        $route = $routeToTrim;
        return substr($route, 0, strpos($route, '?'));
    }

    /**
     * @param null $method
     */
    public function setMethod($method): void
    {
        $this->method = $method;
        $this->path['method'] = $method;
    }

    /**
     * @param mixed $controllerPath
     */
    public function setControllerPath($controllerPath): void
    {
        $this->controllerPath = $controllerPath;
        $this->path['path'] = $controllerPath;
    }

    /**
     * @param  array|string $param
     */
    public function setParam($param)
    {
        $this->param = $param;
        $this->path['param'] = $param;
    }


}
