<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/8/2019
 * Time: 3:10 PM
 */

namespace lib\DomHouseFramework\Routing;


class ControllerRedirector
{
    /**
     * @var string
     */
    protected $controllerPath;


    /**
     * Redirects to Controller by passing controller path and method to index.php
     * @param string $controllerPath
     * @param string $method
     * @param null $parameters
     * @return bool
     */
    public function toController(string $controllerPath, string $method, $parameters = null)
    {
        $query = http_build_query(array(
           'controller' => $controllerPath,
           'method' => $method
        ));

        header('Location: '.PATH.'?'.$query);
        exit();
        return true;
    }

    /**
     * Redirects to anything
     * @param string $redirection
     * @return bool
     */
    public function toHTTP(string $redirection)
    {
        header("Location: $redirection");
        exit();
        return true;

    }

    /**
     * Redirects to selected Route
     * @param string $route
     * @return bool
     */
    public function toRoute(string $route,$message)
    {
        if ($route[0] === '/') {
            var_dump($route);
            print_r(getallheaders());
            $location = PATH.substr($route, 1);
            if (headers_sent()) {
                var_dump($route);
                exit(header('Location: ' .$location));
                die("Redirect failed. Please click on this link: <a href='".PATH."home'>link</a>");
            }
            else{
                exit(header('Location: ' .$location));
            }


        } else {
            echo 'Route format is wrong : ' . $route . '! <br>
                    Please respect format /route_name';

        }
        return true;
    }

}