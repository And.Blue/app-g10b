<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/28/2019
 * Time: 7:35 PM
 */

namespace lib\DomHouseFramework\Routing;

//authentication
/**
 * Fires methods from classes and instanciates them.
 * Class RouteAction
 * @package lib\DomHouseFramework\Model
 */
class RouteAction
{
    /**
     * Handles the request, links to controllers and fires the methods
     * @param array $path
     */
    function generate(array $path)
    {
        if(!isset($path['path']))
        {
            $path['path'] = $path['controller'];
        }
        //converts the file path to a class
        $path['path'] = (string)str_replace('/', '\\', $path['path']);
        $path['path'] = '\\'.(string)str_replace('.php', '', $path['path']);
        //instanciates Controller
        $controller = new $path['path'];
        $method = isset($path['method']) ? (string)$path['method'] : 'index';
        $param = isset($path['param']) ? $path['param'] : null;
        //Fires the Controller's method
        call_user_func(array($controller, $method),$param);
    }



}