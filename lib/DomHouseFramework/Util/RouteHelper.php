<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/11/2019
 * Time: 3:56 PM
 */

namespace lib\DomFramework\Util;


class RouteHelper
{
    public static function simpleUriDebug($uriElements,$path)
    {
        foreach ($uriElements as $uriElement) {
            if ($uriElement === 'debug') {
                echo 'controllerRoute : ' . $path['path'] . '<br>'.
                    'controllerMethod : '.$path['method'];
            }
        }
    }

}