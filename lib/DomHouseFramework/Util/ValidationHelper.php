<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 5/5/2019
 * Time: 8:14 PM
 */

namespace lib\DomHouseFramework\Util;

/**
 * This is only meant to echo out simple messages. Bundled all of these messages within the same class in order to
 * re-use them everywhere.
 */
trait ValidationOutput
{
    public function displayError(string $errorMessage)
    {
        echo '<div class="isa_warning container"><i class="fa fa-warning"></i>' . $errorMessage . '</div>';
    }

    public function displayWarning(string $warningMessage)
    {
        echo '<div class="isa_warning container"><i class="fa fa-warning"></i>' . $warningMessage . '</div>';
    }

    public function displaySuccess(string $successMessage)
    {
        echo '<div class="isa_warning container"><i class="fa fa-warning"></i>'.$successMessage.'</div>';
    }
}

class ValidationHelper
{
    use ValidationOutput;

}