<?php
namespace lib\DomHouseFramework\Util;


/**
 * Class JsonHelper
 * Methods that help with the handling of JSON files
 * @package lib\DomFramework\Util
 */
class JsonHelper
{

    /**
     * From a .json file gets a proper php dictionary
     * @param string $jsonFileName
     * @return array
     */
    public static function getArrFrom(string $jsonFileName) : array
    {
        //gets the routes from routes.php
        $jsonFile = file_get_contents($jsonFileName);
        return self::convertStdToArray(json_decode($jsonFile));

    }

    /**
     * Converts the JSON to a proper dictionary the input is an array of StdClass
     * The value of the dictionnary is an array with path and method if it exists
     * static
     * @param \stdClass $in
     * @return array
     */
    public static function convertStdToArray(\stdClass $in): array
    {

        $out = [];
        foreach ($in as $key => $route) {
            $out[$key] = $route;
        }

        return $out;
    }
}
