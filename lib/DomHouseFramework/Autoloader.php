<?php
namespace lib\DomHouseFramework;
/**

 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/21/2019
 * Time: 7:39 PM
 */

/**
 * Class Autoloader
 * This class is key to to an MVC structure, instead of having to put a "require" for every class you need in your projet
 * (which results sometimes for dozens of classes so dozens of requires) the autoloader does the job and provides the classes
 * used within the project
 * @package lib\DomHouseFramework\
 */
class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($class) {
            $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
            if (file_exists($file)) {
//                echo 'Autoloader::register loaded : '.$file.'<br>';
                require $file;
                return true;
            }
            return false;
        });
    }

//  A vain and naive autoloader that probably doesn't work, many thanks to our dear spl_autoload_register
//
//    /**
//     * @param $class loads class
//     */
//    static function autoload($class)
//    {
//        echo "trying to be loaded: " . $class;
//        require 'class/' . $class . '.php';
//    }
//
//    /**
//     * Searches the files to load within a wanted dirname
//     * @param string $dirname
//     * @throws Exception
//     */
//    static function loadDir(string $dirname)
//    {
//        $arr = self::readThrough();
//
//        foreach ($arr as $item) {
//            if (preg_match("/{$dirname}/i", $item)) {
//                require '' . $item . '';
//                echo 'loaded : ' . $item . '<br>';
//            }
//        }
//    }
//
//    /**
//     * UNUSED
//     * Gets the subdirectories of directories so they can get required in autoload
//     * @return array|null
//     * @throws Exception
//     */
//    private static function readThrough(): ?array
//    {
//        /**
//         * Really not happy with this haha
//         * For now doesn't directly autoload sub  directories
//         */
//        require 'registeredBundles.php';
//        try {
//            $phpFiles = array();
//
//            if (isset($registered)) {
//                foreach ($registered as $dir) { //gets all the registered files and searches subdirs and outputs only the .php files
//
//                    foreach (scandir($dir) as $sub) {
//                        if (!($sub === '.') && !($sub === '..')) {
//                            if (isset(pathinfo($sub)['extension']) && pathinfo($sub)['extension'] == 'php') {
//                                $phpFiles[] = $dir . $sub;
//
//                            }
//                        }
//                    }
//                }
//                var_dump($phpFiles);
//                return $phpFiles;
//
//            } else {
//                throw new Exception("The registered routes are not found");
//
//            }
//        } catch (Exception $exception) {
//            throw $exception;
//        }
//    }
}