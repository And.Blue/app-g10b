<style>
    html {
        height: 100%;
    }

    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    .footer {
        position: absolute;
        width: 100%;
        height: 10%;
        margin-top: 50px;

    }

    .container {
        min-height: 100%;
        position: relative;
    }

    .fontText {
        font-size: 13px;
        font-family: Roboto;
        color : #707070;
        text-align : left;
    }



</style>

<div class="footer">
    <img src="<?=PATH?>public/img/domhouse_s.png" align="left">
    <a href="home" class="fontText">A propos de notre équipe</a>
    <br>
    <?= isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER' ?
    '
    <a href="'.PATH.'announce" class="fontText">Annonces</a>
    <br>' : ''?>

    <a href="<?=PATH?>mentions-legales" class="fontText">Mentions légales</a>
    <br>
    <a href="<?=PATH?>cond_utilisations" class="fontText">Conditions d'utilisation</a>
    <br>

</div>
