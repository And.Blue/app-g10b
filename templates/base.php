<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--    FONTS-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!--    ICONS
    https://fontawesome.com/v4.7.0/examples/
    -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--    CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <script src="public/js/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="<?= PATH ?>public/css/style1.css">

    <title>
        <?php
        echo(isset($title) ? $title : "SET TITLE");
        ?> </title>

</head>
<body>

<!--NAVBAR-->
<?php
if (isset($_SESSION['role']) && $_SESSION['role'] == 'ROLE_ADMIN') {

    require 'templates/Admin/AdminPanel.php';

} else {
    require 'navbar/navbar1.php';
    ?>
    <!--CONTENT-->
    <div class="container">
        <?php

        //this displays the client navigation menu
        if (isset($_SESSION['role']) && $_SESSION['role'] == 'ROLE_USER') {
            require('templates/navbar/clientNavbar.php');
        }
        ?>
        <?php echo $content; ?>
    </div>
    <?php
}
require 'templates/footer/footer.php';
?>


</body>
</html>
