<?php $title='Home';?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        <meta name="viewport" content="width=device-width, initial-scale=1">

                                                                           div#slider { width: 80%; max-width: 1000px; }

        div#slider figure {
            position: relative;
            width: 500%;
            margin: 0;
            padding: 0;
            font-size: 0;
            text-align: left;
            animation: 10s slidy infinite;
        }
        div#slider figure img { width: 20%; height: auto; float: left; }
        div#slider { width: 80%; max-width: 1000px; overflow: hidden }

        @keyframes slidy {
            0% { left: 0%; }
            20% { left: 0%; }
            25% { left: -100%; }
            45% { left: -100%; }
            50% { left: -200%; }
            70% { left: -200%; }
            75% { left: -300%; }
            95% { left: -300%; }
            100% { left: 0%; }
        }

        img.resize {
            padding  : 0px 200px;
            width : 55%;
            height : auto;
        }

        .font {
            font-size: 22px;
            font-family: Roboto;
            font-style : italic;
            color : #707070;
            height : 18px;
            padding : 40px 200px;
        }

        .fontTitle{
            font-size : 40px;
            font-family: Roboto;
            font-weight : bold;
            color : #0097A7;
            text-align : left;
        }

        .fontTitle2{
            font-size : 40px;
            font-family: Roboto;
            font-weight : bold;
            color : #0097A7;
            text-align : left;


        }

        .fontText {
            font-size: 20px;
            font-family: Roboto;
            color : #707070;
            text-align : left;
        }

        /*
        img.resizeGrid {

            height : auto;
            padding : 11px 10px;
        }

        card.resizeGrid {

            height :auto;
            padding : 11px 10px;
        }

    */












        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 85%;

            bottom: 100px;


        }

        .cardContact {

            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 75%;

            bottom: 100px;
        }

        .cardContact:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }



        .column {
            float: left;
            width: 30.33%;
            padding: 10px;
        }

        .columnContact{

            width: 90%;
            padding: 20px 50px 50px 200px;

        }

        .imgCard{

            float : right;
            position: absolute;
            margin-left: 220px;
            margin-top: 28px;
        }

        .imgCard2{
            float : right;
            position: absolute;
            margin-left: 613px;
            margin-top: 41px;
        }

        .imgCard3{
            float : right;
            position: absolute;
            margin-left: 1000px;
            margin-top: 41px;
        }

        .imgCard4{
            float : right;
            position: absolute;
            margin-left: 219px;
            margin-top: 234px;
        }

        .imgCard5{
            float : right;
            position: absolute;
            margin-left: 1000px;
            margin-top: 234px;
        }




        /* Clearfix (clear floats) */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }

        .centerDH {
            text-align : center;
            padding-top : 35px;
        }

        .fontText2 {
            font-size: 12.5px;
        }

        .imgContact{
            width : 10%;
            height: auto;

            margin-top: 50px;

            padding-top: 30px;
        }

        .textPosition {
            font-size: 19px;
            margin-left: 50px;
        }


        input[type=text], select, textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            margin-top: 6px;
            margin-bottom: 16px;
            resize: vertical;


        }

        input[type=submit] {
            max-width : 15%;
            background-color: #0097A7;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-left: 85%;

        }

        input[type=submit]:hover {
            background-color: #0097A7;

        }

        .container1 {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;


        }




        .caption-style-1{
            list-style-type: none;
            margin: 0px;
            padding: 0px;

        }

        .caption-style-1 li{
            float: left;
            padding: 25px;
            position: relative;
            overflow: hidden;
        }

        .caption-style-1 li:hover .caption{
            opacity: 1;

        }


        .caption-style-1 img{
            margin: 0px;
            padding: 0px;
            float: left;
            z-index: 4;
        }


        .caption-style-1 .caption{
            cursor: pointer;
            position: absolute;
            opacity: 0;
            -webkit-transition:all 0.45s ease-in-out;
            -moz-transition:all 0.45s ease-in-out;
            -o-transition:all 0.45s ease-in-out;
            -ms-transition:all 0.45s ease-in-out;
            transition:all 0.45s ease-in-out;

        }
        .caption-style-1 .blur{
            background-color: rgba(0,0,0,0.65);
            height: 300px;
            width: 400px;
            z-index: 5;
            position: absolute;
        }

        .caption-style-1 .caption-text h1{
            text-transform: uppercase;
            font-size: 24px;
        }
        .caption-style-1 .caption-text{
            z-index: 10;
            color: #fff;
            position: absolute;
            width: 400px;
            height: 300px;
            text-align: center;
            top:100px;
        }





    </style>
</head>
<center>
    <br>
    <img src="<?=PATH?>public/img/DomHouseBig.jpg" class="resize">

    <center class="font">L'excellence de la domotique à moins de 3 clics</center>
    <center>
        <div id="slider">
            <figure>

                <img src="<?=PATH?>public/img/austin-fireworks.jpg">
                <img src="<?=PATH?>public/img/ibiza.jpg">
                <img src="<?=PATH?>public/img/taj-mahal.jpg">
                <img src="<?=PATH?>public/img/ankor-wat.jpg">

            </figure>
        </div>
    </center>
    <p class="fontTitle">Qui sommes nous ?</p>
    <p class="fontText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ligula nisi, aliquet vitae bibendum at, congue sed nulla. Aliquam sed elit ac ipsum accumsan venenatis malesuada quis neque. Aliquam non tellus ac sapien pretium blandit aliquet non sapien. Vestibulum a commodo neque, vel interdum neque.</p>

    <p class="fontTitle">Nos différentes solutions</p>
    <p class="fontText">Grille des différents produits DomHouse qui peuvent être ajoutés chez soi </p>


    <div class="container-a1">
        <ul class="caption-style-1">
            <li>
                <img src="<?=PATH?>public/img/capteur1.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de mouvement</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur2.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de température</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur3.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de température</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur4.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de mouvement</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur5.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de mouvement</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur6.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur d'humidité</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur7.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur d'humidité</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur8.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur d'humidité</h1>
                        <p></p>
                    </div>
                </div>
            </li>
            <li>
                <img src="<?=PATH?>public/img/capteur9.jpg" alt="">
                <div class="caption">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <h1>Capteur de température</h1>
                        <p></p>
                    </div>
                </div>
            </li>
        </ul>
    </div>



</center>

<p class="fontTitle2">Notre équipe</p>

<div class="row">
    <div class="column">
        <img src="<?=PATH?>public/img/nico.jpg" style="width:8%" class="imgCard" align="right">
        <img src="<?=PATH?>public/img/andrew.jpg" style="width:8%" class="imgCard2" align="right">
        <img src="<?=PATH?>public/img/lucas.jpg" style="width:8.5%" class="imgCard3" align="right">
        <img src="<?=PATH?>public/img/narindra.jpg" style="width:8%" class="imgCard4" align="right">
        <img src="<?=PATH?>public/img/nico.jpg" style="width:8%" class="imgCard5" align="right">
        <div class="card">

            <div class="container">

                <h3><b>Nicolas</b></h3>
                <p>Développeur</p>
                <a class="fontText2">nicolas.evesque@isep.fr</a>

            </div>
        </div>
    </div>
    <div class="column">
        <div class="card">
            <div >
            </div>
            <div class="container">
                <h3><b>Andrew</b></h3>
                <p>Développeur</p>
                <a class="fontText2">andrew.pouret@isep.fr</a>

            </div>
        </div>
    </div>
    <div class="column">
        <div class="card">
            <div class="container">
                <h3><b>Lucas</b></h3>
                <p>Développeur</p>
                <a class="fontText2">lucas.cuzeau@isep.fr</a>

            </div>
        </div>
    </div>
    <div class="column">
        <div class="card">
            <div class="container">
                <h3><b>Narindra</b></h3>
                <p>Développeur</p>
                <a class="fontText2">narindra.razakarivony@isep.fr</a>

            </div>
        </div>
    </div>
    <div class="column">
        <div class="centerDH">
            <img src="<?=PATH?>public/img/domhouse_s.png" style="width:50%">

        </div>
    </div>
    <div class="column">
        <div class="card">
            <div class="container">
                <h3><b>Julie</b></h3>
                <p>Développeur</p>
                <a class="fontText2">julie.caudroit@isep.fr</a>

            </div>
        </div>
    </div>
</div>

<p class="fontTitle2">Contact</p>

<div class="columnContact">
    <div class="cardContact">
        <div class="container">

            <div class="imgContact">
                <img src="<?=PATH?>public/img/mail.png" align="left" style="width:50%">
            </div>

            <div class="textPosition">
                <a>adresse@isep.fr</a>
            </div>
            <br>
            <div class="imgContact">
                <img src="<?=PATH?>public/img/tel.png" align="left" style="width:50%">
            </div>

            <div class="textPosition">

                <a>tel</a>

            </div>
            <br>
            <div class="imgContact">
                <img src="<?=PATH?>public/img/localisation.png" align="left" style="width:50%">
            </div>
            <div class="textPosition">
                <a>position</a>
            </div>


            <br>
            <br>
            <br>




            <form action="/action_page.php">
                <label for="fname">First Name</label>
                <input type="text" id="fname" name="firstname" placeholder="Your name..">

                <label for="lname">Last Name</label>
                <input type="text" id="lname" name="lastname" placeholder="Your last name..">

                <label for="subject">Subject</label>
                <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

                <input type="submit" value="Submit">
            </form>





        </div>

    </div>
</div>























