
<h1>Toutes les annonces</h1>

<div style="text-align: center; color: lightslategrey">
    <h2>Bonjour <?= $_SESSION["user"]["name"] . ' ' . $_SESSION["user"]["surname"]?> !</h2>
</div>
<div class="announce_container">
    <?php
    $title= 'Annonces';

    echo '   
    <button class="slider1" onclick="sliderUp()"><i class="fa fa-arrow-circle-right"></i></button>
';
    foreach ($templateData as $info) {
        echo '<div class="card-container">
        
        <div class="card-big u-clearfix">
            <div class="card-body">
                <span class="card-number card-circle subtle">' . $info['id_announcement'] . '</span>
                <h2 class="card-title">' . $info['title'] . '</h2>
                <span class="card-author subtle"><em>' . $info['surname'] . ' ' . $info['name'] . ' - modifié le ' . $info['modif_at'] . '</em></span>
                <span class="card-description subtle">' . $info['text'] . '</span>
                <div class="card-read"></div>
                
            </div>
        
        </div>
        
        <div class="card-shadow"></div>
        
    </div>

';
    }
    echo '            <button class ="slider2" onclick="sliderDown()"><i class="fa fa-arrow-circle-left"></i></button>

';
    ?>
</div>

<style>

    .slider1, .slider2{
        width: 30px;
        height: 100px;
        background-color: grey;
    }

    .announce_container{
        position: relative;
        left: 18%;

        padding: 90px;
    }
    .announce_container .slider2{

        position: absolute;
        left: 0;
        top: 165px;
    }
    .announce_container .slider1{

        position: absolute;
        left: 750px;
        top: 165px;

    }
    .card-container{
        position: absolute;
        margin: 30px;
        left: 30px;

    }


</style>
<script>

    var index = 0;
    div = document.getElementsByClassName("card-container");
    for (i = 0; i<div.length; i++){
        if(i===index){
            div[i].style.display = 'block';
        }
        else{
            div[i].style.display = 'none';
        }
    }


    function sliderUp() {
        console.log('executed');
        console.log(index);
        var  div, i;
        div = document.getElementsByClassName("card-container");
        if(index != div.length-1){
            index++;
        }
        else{
            index = 0;
        }
        for (i = 0; i<div.length; i++){
            if(i===index){
                div[i].style.display = 'block';
            }
            else{
                div[i].style.display = 'none';
            }
        }
    }

    function sliderDown() {
        console.log('executed');
        console.log('index');
        var  div, i;
        div = document.getElementsByClassName("card-container");
        console.log(div.length);
        if(index !=0){
            index--;
        }
        else{
            index = div.length-1;
        }
        for (i = 0; i<div.length; i++){
            if(i!=index){
                div[i].style.display = 'none';
            }
            else{
                div[i].style.display = 'block';
                console.log(i);
            }
        }
    }

</script>
