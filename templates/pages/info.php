<?php $title = 'Informations'; ?>
<style>
    .name, .surname, .address, .email{
        background-color: white;
        border: 1px solid;
        border-radius: 4px;
        padding : 8px;
        margin-top: 6px;
        width: 95%;
        display: inline-block;
        font-size: medium;
    }

    .infos{
        margin: auto;
        padding: 10px;
        border: 1px black solid;
        width: 400px;
        border-radius: 5px;
        text-align: left;
        background-color: ghostwhite;
        box-shadow: 3px 3px 3px grey;
    }

    .infos p{
        margin: 20px;
        font-size: large;
    }

</style>

<h1>Profil</h1>

<?php
if(isset($message) && $message =='is_not_logged'){
    echo '<p>Afin de consulter/modifier vos données personnelles, '.'<a href="'.PATH. 'login">Cliquez ici</a>'.' pour vous connecter</p>';
}
else{
    echo '<p>Voici vos informations personnelles</p>';
}
?>
<div class="infos">
    <p>Nom :<span class="name"><?php echo $templateData['name']?><span/><p/>
    <p>Prenom :<span class="surname"><?php echo $templateData['surname']?><span/><p/>
    <p>Adresse :<span class="address"><?php echo $templateData['address']?><span/><p/>
    <p>Email :<span class="email"><?php echo $templateData['email']?><span/><p/>
    <?php
    if(isset($_SESSION['logged']) && $_SESSION['logged']) {
        echo '<a class="button green" href="' . PATH . 'user/selfEdit?' . $_SESSION['user']['id_user'] . '"><span class="">Editer</span></a>'
        ;}
    ?>
</div>