<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 23/05/2019
 * Time: 09:44
 */
$title = 'My house ';

?>
<style>
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
    }

    /* Add rounded corners to the top left and the top right corner of the image */
    img {
        border-radius: 5px 5px 0 0;
    }

</style>

<h1>Mes habitations</h1>
<?php
if (isset($templateData['houses']) && sizeof($templateData['houses']) > 1 ) {
    foreach ($templateData['houses'] as $house) {
        echo '
         <div class="card">
  <div><i class="fa fa-home"></i></div>
  <div class="container">
    <h4><b>' . $house['house_name'] . '</b></h4>
    <p>' . $house['house_address'] . ', <em>' . $house['house_postal'] . '</em></p>
 <a href="' . PATH . 'my-home?' . $house['house_room'] . '" class="button"><span>Enter</span></a>
  </div>
</div> 
        ';
    }

} else {
    echo '<div style = "color: grey;text-align: center" >
    <h2 > Pas encore de maison.</h2 >
    <i style="font-size: 10rem" class="fa fa-home"></i>
    <h3 style = "font-weight: lighter" > Cliquez sur + pour ajouter une habitation </h3 >
</div >
 ';
}
?>


<a class="button-add" href="<?= PATH ?>my-house-add">+</a>