<?php

/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 30/04/2019
 * Time: 11:06
 */

$_SESSION['id_room'] = $templateData['id_room']['id_room'];
$_SESSION['id_house'] = $templateData['id_house']['id_house'];
?>


<h2>Ajoutez votre capteur</h2>

<a style="text-decoration: none; margin-bottom: 10px" href="<?= PATH.'my-home?'.$_SESSION['id_home']?>"><i class="fa fa-chevron-circle-left"></i> Retour vers toutes mes pièces</a>
<br>
<div class="form-window" style="">
    <form action="<?= PATH ?>sensor_add" method="post">
        <label for="name"><br/>Nom du capteur</label>
             <input id="name" type="text" name="name" placeholder="Ex : Taral">
        <label for="sensor-type">Type de capteur</label><br>
            <select name="sensor-type" id="sensor-type">
                <option value="---">---</option>
            </select><br>

        <div id="test"></div>
        <button class="button" type="submit" style="vertical-align:middle; color: white"><span>+</span></button>
        <?php echo '<a href="' . PATH . 'home_sensor_add'.'" class="button"><span>Vue</span></a>'; ?>

    </form>

</div>


<a href="<?=PATH.'test'?>">test</a>


<script type="module">


</script>
<script src="public/js/client/addSensor.js"></script>

