<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 03/05/2019
 * Time: 22:58
 */
$title = "Ajouter Capteur/Actionneur";
$count = 0;
?>


<h2>Bienvenue chez Vous </h2>
<style>


    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }


    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }


    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .onoff {

        margin-bottom: 10%;
        margin-top: -4%;
        margin-left: 50%;

    }

    /*margin-top : -7%;/

     */
    .buttonEdit {
        display: inline-block;

        font-size: 100px;
        cursor: pointer;
        text-align: center;
        text-decoration: none;
        outline: none;
        color: #fff;
        background-color: #1387af;
        border: none;
        box-shadow: 0 9px #999;
        margin: 50% 80%
    }

    .buttonEdit {
        border-radius: 65%;
    }

    .buttonEdit:hover {
        background-color: #3e8e41
    }

    .buttonEdit:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }

    #Edit {
        margin-left: 81%;
        margin-top: -48%;
    }


</style>

<a style="text-decoration: none; padding-bottom: 20px" href="<?= PATH.'my-home?'.$_SESSION['id_home']?>"><i class="fa fa-chevron-circle-left"></i>Ma pièce</a>

<?php

foreach ($templateData['sensors'] as $mySensor) {
    if ($mySensor['composant_type'] == 'ACT_VALUE' && $_SESSION['id_room'] == $mySensor['composant_id_room']) {
        $count++;

        echo ' 
 <div class="act" >
            <img src="public/img/windows_icon.png" alt="sensor1" style="max-width: 50px" class="actionneur">' . '<p>' . $mySensor['composant_nom'] . '</p>
           <div class="onoff">
               <label class="switch">
                        <input type="checkbox">
                     <span class="slider round"></span>
                </label>
                   <td><a href="' . PATH . 'sensor_delete?' . $mySensor['composant_id'] . '" class="button"><span>Supprimer</span></a></td> 
                   <td><a href="' . PATH . 'my-home/sensor/edit?' . $mySensor['composant_id'] . '" class="button"><span>Modifier</span></a></td>
                   
            </div>
         
            </td>';

    }

}

?>
<?php
if (isset($_SESSION['id_room'])) {

    foreach ($templateData['sensors'] as $mySensor) {
        if ($mySensor['composant_type'] == 'CAPT_VALUE' && $_SESSION['id_room'] == $mySensor['composant_id_room']) {
            $count++;
            echo ' <img src="public/img/sensor.png" alt="sensor1" style="max-width: 60px" id="sensor1">' . $mySensor['composant_nom'] . '<br>';
            echo '
    <div id=' . $mySensor['composant_id'] . ' class="200x150px" style="max-width:130px "></div>
             <script src="justgage-1.2.2/raphael-2.1.4.min.js"></script>
            <script src="justgage-1.2.2/justgage.js"></script>
            <script>
            var name = "' . $mySensor["composant_nom"] . '";
            var id_compo = "' . $mySensor["composant_id"] . '";
            var g = new JustGage({
                id: id_compo,
                value: 45,
                min: 0,
                max: 100,
                title:name,
                gaugeWidthScale : 0.1,
                Color : "#9999ff"    
            })
            
            </script>
            <td><a href="' . PATH . 'sensor_delete?' . $mySensor['composant_id'] . '" class="button"><span>Delete</span></a></td>
            <td><a href="' . PATH . 'my-home/sensor/edit?' . $mySensor['composant_id'] . '" class="button"><span>Modifier</span></a></td>
                    ' . '<br>';


        }

    }
}

if($count === 0) {
    echo '<div style = "color: grey;text-align: center" >
    <h2 > Pas encore de capteurs ou actionneurs</h2 >
    <i style="font-size: 10rem" class="fa fa-plus-circle"></i>
    <h3 style = "font-weight: lighter" > Cliquez sur + pour en ajouter un </h3 >
</div >
 ';
}
?>


<a class="button-add" href="<?= PATH?>index_home?<?=$_SESSION['id_specific_room']?>">+</a>




