<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 16/05/2019
 * Time: 22:25
 */
/*$title = 'Modifier '.$templateData['conom'];
var_dump($templateData  )*/?>



<h1>Modifier <?=$templateData['nom']?></h1>
<?php
/*echo isset($message) ? '<div class="isa_success"><i class="fa fa-check"></i>'.$message.'</div>' : '';

*/?>
<form action=" " method="post" >
    <label for="nom">Nom du composant </label>
    <input id="nom" type="text" name="nom" value="<?=$templateData['nom']?>" placeholder="Nom">
    <button class="" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span></button>
</form>



<script>
    const select = document.getElementById('sensor-type');



    function ajaxGet(toUrl, callback) {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                return callback(JSON.parse(this.response));
            } else {
                return {error: 'Ressource not founderror'};
            }
        };
        http.open("GET", toUrl, true);
        http.send();
    }

    function addOption(select, option) {
        let optionNode = select.appendChild(
            document.createElement('option')
        );
        optionNode.appendChild(
            document.createTextNode(option.text)
        );
        optionNode.value = option.value;
    }

    ajaxGet('/appinfo/get_sensortypes', (data) => {
        // console.log(data);
        data.map(option => {
                addOption(select, {text: option.name, value: option.id_composant_type});
            }
        );
    });

</script>
