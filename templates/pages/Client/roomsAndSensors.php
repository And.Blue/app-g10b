<?php
$title = 'Chez moi';
?>

<style>
    .sensor-style {
        font-size: 1.3rem;
        color: grey;
    }


</style>

<div id="app-myhome">
    <div class="tabs">
        <ul>
            <li class="one" @click="isTab1 ? '' : isTab1 = !isTab1"><a>Mes Pièces</a></li>
            <li class="two" @click="!isTab1 ? '' : isTab1 = !isTab1"><a>Mes Capteurs</a></li>
            <hr :style="[isTab1 ? styleTab1 : styleTab2]">
        </ul>
    </div>

    <div class="tabs-content">
        <div v-if="isTab1">
            <?php
            //room and sensors
            require 'templates/pages/Client/myHome.php';
            ?>
        </div>
        <div v-else>
            <div class="grid-container">

                <div v-for="(room, index) in rooms">
                    <room-card class="card-author" v-bind:room="room"></room-card>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="public/js/client/myHome.js"></script>
<script>
    const appMyHome = new Vue({
        el: '#app-myhome',
        data: {
            isTab1: true,
            styleTab1: {
                marginLeft: '10%'
            },
            styleTab2: {
                marginLeft: '50%'
            },
            rooms: []
        },
        created: function () {
            this.fetchSensors();
            setInterval(this.fetchSensors, 2000)
        },
        methods: {
            fetchSensors: function () {
                axios
                    .get('/appinfo/api/sensors/home?3')
                    .then(res => {
                        let rooms = [];
                        Object.values(res.data.data).map(room => {
                            rooms.push(room);
                        });
                        this.rooms = rooms;
                        console.log(this.rooms[0].sensors[0].value_component);
                    })
                    .catch(err => console.log(err));

            }

        },
        components: {
            'room-card': Room
        }
    });

</script>

