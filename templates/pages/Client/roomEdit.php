<?php $title = 'Modifier '.$templateData['name'];
?>



<h1>Modifier <?=$templateData['name']?></h1>


<a style="text-decoration: none; margin-bottom: 10px" href="<?=PATH?>my-home?<?=$_GET['home']?>"><i class="fa fa-chevron-circle-left"></i> Retour à la page Chez Moi</a>

<?php
echo isset($message) ? '<div class="isa_success"><i class="fa fa-check"></i>'.$message.'</div>' : '';

?>
<form action="" method="post" style="margin-top: 10px">
    <label for="name">Nom de la Pièce</label>
    <input id="name" type="text" name="name" value="<?=$templateData['name']?>" placeholder="Nom">

    <button class="" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span></button>
</form>



<script>
    const select = document.getElementById('room-type');


    function ajaxGet(toUrl, callback) {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                return callback(JSON.parse(this.response));
            } else {
                return {error: 'Ressource not founderror'};
            }
        };
        http.open("GET", toUrl, true);
        http.send();
    }

    function addOption(select, option) {
        let optionNode = select.appendChild(
            document.createElement('option')
        );
        optionNode.appendChild(
            document.createTextNode(option.text)
        );
        optionNode.value = option.value;
    }

    ajaxGet('/appinfo/get_roomtypes', (data) => {
        // console.log(data);
        data.map(option => {
                addOption(select, {text: option.name, value: option.id_room_type});
            }
        );
    });


</script>

