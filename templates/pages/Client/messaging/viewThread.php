<?php
$messageNb = sizeof($templateData);
$count = 1; //in order to sort messages for the frontend.

$title = 'Conversation: ' . $templateData[0]['title']; ?>

<h1>Conversation: <?= $templateData[0]['title'] ?></h1>
<p>Les conversations sont là afin de rassembler des messages</p>
<a style="text-decoration: none" href="<?= PATH ?>admin/thread/all"><i class="fa fa-chevron-circle-left"></i> Retour à
    la liste de toutes les conversations</a>

<style>
    .fa-trash:hover {
        color: white;
        background: darkred;
        border-radius: 50px;
        padding: 2px;
        cursor: pointer;
    }
</style>

<div>
    <?php
    foreach ($templateData as $message) {
        $reply = $count === 1 ? '<span class="right-side" onclick="messageDrawer(lastMessage)"><i class="fa fa-reply" style="cursor: pointer"></i></span>' :
            '<span class="right-side" style="color: darkred"><a onclick="confirm(`Voulez-vous réelement supprimer ce message?`)" href="' . PATH . 'delete_message?' . $message['id_message'] . '" style="text-decoration: none;color: inherit"><i class="fa fa-trash"></a></i></span>';

        $injectId = $count === 1 ? 'id="last-message"' : '';
        $nb = $messageNb - $count + 1;
        echo '<div class="card-container" ' . $injectId . '>
        <div class="card-big u-clearfix">
        <div class="card-head">
         ' . $reply . '
        <span class="left-side"><i class="fa fa-envelope"></i> n°' . $nb . '</span>
        </div>
            <div class="card-body">  
                <span class="card-description">' . $message['content'] . '</span>
                <span class="card-author subtle"><em>' . $message['author_surname'] . ' ' . $message['author_name'] . ' a envoyé ce message à ' . $message['recipient_surname'] . ' ' . $message['recipient_name'] . '  le ' . $message['sent_at'] . '</em></span>
                <div class="card-read"></div>
<!--                <span class="card-tag card-circle subtle">C</span>-->
            </div>
        </div>
        <div class="card-shadow"></div>
    </div>
';
        $count++;
    }
    ?>
</div>


<script src="<?= PATH ?>public/js/messaging/viewThread.js"></script>



