<?php $title = 'Mes messages'; ?>
<style>
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
    }

    /* Add rounded corners to the top left and the top right corner of the image */
    img {
        border-radius: 5px 5px 0 0;
    }

</style>
<h1>Tous mes messages</h1>
<?php

if (isset($templateData)) {
    if (sizeof($templateData) < 1) {
        echo '
<div style = "color: grey;text-align: center" >
    <h2 > Pas encore de messages...</h2 >
    <i style = "font-size: 10rem" class="fa fa-inbox" ></i >
    <h3 style = "font-weight: lighter" > Cliquez sur + pour créer une conversation </h3 >
</div > ';
    } else {
        foreach ($templateData as $thread) {
            echo '
         <div class="card">
  <div><i class="fa fa-envelope"></i></div>
  <div class="container">
    <h2>' . $thread["topic"] . '</h2><p style="font-weight: lighter;color: lightslategrey"><em>Entre ' . $thread["author_name"] . ' ' . $thread["author_surname"] . ' et '
                . $thread["recipient_name"] . ' ' . $thread['recipient_surname'] . ' </em></p>
    <a href = "' . PATH . 'conversation?' . $thread['id_conv'] . '" class="button" ><span >Voir</span ></a >
  </div>
</div > ';
        }
    }
}

?>

<a class="button-add" href="<?= PATH ?>conversation/new">+</a>

