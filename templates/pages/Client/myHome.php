<?php $title = 'Chez Moi';
$_SESSION['id_house_room'] = $templateData['id_house_room'];
$homeId = key($_GET);
$_SESSION['id_home'] = $homeId;
$count = 0;
?>
<h2>Pièces</h2>

<a style="text-decoration: none; margin-bottom: 10px" href="<?= PATH ?>my-house"><i
            class="fa fa-chevron-circle-left"></i> Retour vers toutes mes maisons</a>

<?php
if (isset($templateData['rooms']) && isset($templateData['id_house_room'])) {

    ?>
    <table style="margin-top: 10px">
        <tr>
            <th>#</th>
            <th>Type</th>
            <th>Nom</th>
            <th>Action</th>
            <th>Ajouter</th>
        </tr>
        <?php
        foreach ($templateData['rooms'] as $room) {
            if ($_SESSION['id_house_room']['id_house_room'] == $room['room_house']) {
                echo '<tr><td>' . $room['id_room'] . '</td><td>' . $room['type_name'] . '</td><td>' . $room['room_name'] . '</td><td><a href="' . PATH . 'my-home/room/edit?' . $room['id_room'] . '&home=' . $homeId . '" class="button"><span>Modifier</span></a>
<a href="' . PATH . 'room_delete?' . $room['id_room'].'" class="button"><span>Supprimer</span></a>
</td>
        <td><a href="' . PATH . 'index_home?' . $room['id_room'] . '" class="button"><span>Add</span></a></td>
</tr>';
                $count++;
            }

        }
        ?>
    </table>
    <?php

    if ($count == 0) {
        echo '<div style = "color: grey;text-align: center" >
    <h2 > Pas encore de Pièces.</h2 >
    <i style="font-size: 10rem" class="fa fa-plus-circle""></i>
    <h3 style = "font-weight: lighter" > Ajoutez en tout de suite ! </h3 >
</div >
 ';
    }
}


?>
<h2>Ajout Pièce</h2>
<div class="form-window">
    <form action="<?= PATH ?>room_add" method="post">
        <label for="name"><br/>Nom de la Pièce</label>
        <input id="name" type="text" name="name" placeholder="Ex : Salon, Salle à manger">
        <label for="room-type">Type de Pièce</label><br>
        <select name="room-type" id="room-type">
            <option value="---"> ---</option>
        </select><br>
        <div id="test"></div>
        <button class="" type="submit" style="vertical-align:middle; color: white"><span>+</span></button>
    </form>
</div>

<script>
    const select = document.getElementById('room-type');


    function ajaxGet(toUrl, callback) {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                return callback(JSON.parse(this.response));
            } else {
                return {error: 'Ressource not founderror'};
            }
        };
        http.open("GET", toUrl, true);
        http.send();
    }

    function addOption(select, option) {
        let optionNode = select.appendChild(
            document.createElement('option')
        );
        optionNode.appendChild(
            document.createTextNode(option.text)
        );
        optionNode.value = option.value;
    }

    ajaxGet('/appinfo/get_roomtypes', (data) => {
        // console.log(data);
        data.map(option => {
                addOption(select, {text: option.name, value: option.id_room_type});
            }
        );
    });


</script>
