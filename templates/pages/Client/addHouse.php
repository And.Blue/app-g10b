<?php $title='Ajouter Votre Maison';
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 23/05/2019
 * Time: 11:05
 */

?>
<h2>Ajouter votre maison</h2>
<div class="form-window">
    <form action="<?= PATH ?>house_add" method="post">
        <label for="house-type">Type de maison</label><br>
        <select name="house-type" id="house-type">
            <option value="---">---</option>
        </select><br>
        <label for="address"><br/>Address :</label>
        <input id="address" type="text" name="address" placeholder="34 rue Jean Moulin..">
        <label for="code_postal"><br/>Code-postal :</label>
        <input id="code_postal" type="text" name="code_postal" placeholder="75017">
        <div id="test"></div>
        <button class="buttonAdd" type="submit" style="vertical-align:middle; color: white"><span>+</span></button>
    </form>
</div>

<script>
    const select = document.getElementById('house-type');



    function ajaxGet(toUrl, callback) {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                return callback(JSON.parse(this.response));
            } else {
                return {error: 'Ressource not founderror'};
            }
        };
        http.open("GET", toUrl, true);
        http.send();
    }

    function addOption(select, option) {
        let optionNode = select.appendChild(
            document.createElement('option')
        );
        optionNode.appendChild(
            document.createTextNode(option.text)
        );
        optionNode.value = option.value;
    }

    ajaxGet('/appinfo/get_housetypes', (data) => {
        // console.log(data);
        data.map(option => {
                addOption(select, {text: option.name, value: option.id_house});
            }
        );
    });

</script>