<?php $title='Mentions légales';?>

<h1>Mentions légales</h1>
<body>
<h2> Article 1. Informations légales</h2>
<p>
    En vertude l'article 6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance de l'économie numérique, il est précisé
    dans cet article l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi.
</p>
<p>
    Le site domhouse est édité par : Domhouse Corporation, domicilé à l'adresse suivante:
</p>
<p>10 rue de Vanves, Issy-les-Moulineaux 92130</p>
<p>Téléphone : +33xxxxxxxxx / Adresse e-mail : contact@domhouse.fr</p>
<p>Le directeur de publication du site est : M Andrew Pouret</p>
<p>Le site domhouse est hébergé par : ISEP, dont le siège est situé à l'adresse: 10 rue de Vanves, Issy-les-Moulineaux 92130</p>

<h2> Article 2. Accès au site</h2>
<p> Le site est accessible par tout endroit, 7j/7, 24h/24 sauf en cas de force majeure, interruption programmée ou non et pouvant découler d'une necessité de maintenance.
    En cas de modification, interruption ou suspension des services le site Domhouse ne saurait être tenu responsable.

</p>

<h2> Article 3. Collecte des données et cookies</h2>
<p> Le site assure à l'Utilisateur une collecte et un traitement de d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.
    En vertu de la loi informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles.<br>
    Le site ne fait aucun usage de cookies.
</p>

<h2> Article 4. Propriété intellectuelle</h2>
<p>
    Toute utilisation, reproduction, diffusion, commercialisation, modification de tout/de certaines parties du site Domhouse, sans autorisation de l'éditeur est prohibée et pourra entrainer des actions et poursuites judiciaires telles
    que notamment prévues par le Code de la propriété intellectuelle et le Code civil.
</p>



</body>