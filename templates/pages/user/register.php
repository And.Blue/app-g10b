<?php $title = 'Inscription';?>
<style>
    input[type=password], input[type=text]{
        width: 95%;
    }

</style>


<h1>Inscription</h1>
<div id="errorBox"></div>
<div class="registerWindow" id="register" >
    <form action="" method="post" >
        <label for="surname">Prénom</label>
        <input id="surname" type="text" name="surname" placeholder="Prénom">
        <label for="name">Nom</label>
        <input id="name" type="text" name="name" placeholder="Nom">
        <label for="address">Adresse</label>
        <input id="address" type="text" name="address" placeholder="Adresse">
        <label for="city">Ville</label>
        <input id="city" type="text" name="city" placeholder="Ville">
        <label for="postal">Code Postal</label>
        <input id="postal" type="text" name="postal" placeholder="Code Postal">
        <label for="phone">Numéro de Téléphone</label>
        <input id="phone" type="text" name="phone" placeholder="ex: 06 78 91 05 69">
        <label for="email">Email</label>
        <input id="email" type="text" name="email" placeholder="Email">
        <label for="password">Mot-de-Passe</label>
        <input id="password" type="password" name="password" placeholder="Mot-de-Passe">
        <label for="password-conf">Confirmation de Mot-de-passe</label>
        <input id="password-conf" type="password" name="password-conf" placeholder="Confirmation de Mot-de-Passe">

        <label for="cond-util">J'accepte les conditions d'utilisation</label>
        <input type="checkbox" name="cond-util" id="cond-util"><br>
        <span><a href="<?= PATH ?>cond_utilisations"><em>Lien pour consulter les conditions générales d'utilisation</em></a></span>

        <button class="" type="submit" style="vertical-align:middle; color: white"><span>S'inscrire</span></button>

    </form>
</div>

<!--<script>-->
<!--    const errorBox = document.getElementById('errorBox');-->
<!--    const errorStatus = (args) => {-->
<!--        return `<div class="isa_error container"><i class="fa fa-times-circle"></i>${args}</div>`;-->
<!--    };-->
<!---->
<!---->
<!--    function validateForm() {-->
<!--        errorBox.innerHTML = errorStatus('No good man, no good');-->
<!--    }-->

<!--</script>-->