<?php $title="Conditions d'utilisations "?>

<h1>Conditions générales d'utilisation</h1>
<body>
<h2> Article 1. Informations légales</h2>
<p>
    En vertude l'article 6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance de l'économie numérique, il est précisé
    dans cet article l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi.
</p>
<p>
    Le site domhouse est édité par : Domhouse Corporation, domicilé à l'adresse suivante:
</p>
<p>10 rue de Vanves, Issy-les-Moulineaux 92130</p>
<p>Téléphone : +33xxxxxxxxx / Adresse e-mail : contact@domhouse.fr</p>
<p>Le directeur de publication du site est : M Andrew Pouret</p>
<p>Le site domhouse est hébergé par : ISEP, dont le siège est situé à l'adresse: 10 rue de Vanves, Issy-les-Moulineaux 92130</p>

<h2> Article 2. Présentation du site</h2>
<p>Le site domhouse a pour objet : <br/>
    la gestion d'un environnement domotique pour les clients de chez domisep, un utilisateur doit se créer un compte
    afin de pouvoir construire son environnement, il va ainsi définir les différentes habitations dans lesquelles sont présents
    divers capteurs et actionneurs afin d'avoir un accès à eux à distance.<br/>
    L'utilisateur va ainsi pouvoir ajouter différentes pièces et capteurs/actionneurs à une de ses maisons et va pouvoir les controller à distance.
    Il peut récupérer les informations relatives à ses capteurs dans l'onglet "Chez moi", et gérer dans ce même onglet tout son environnement.<br/>
    En cas de soucis techniques / questions, un système de messagerie est présent pour pouvoir contacter directement un administrateur du site.
</p>

<h2> Article 3. Contact</h2>
<p> Pour toute question ou demande d'information concernant le site, ou tout signalement de contenu ou d'activités illicites,
    l'utilisateur peut contacter l'éditeur à l'adresse e-mail suivante : contact@domhouse.fr ou adresser un courrier recommandé avec accusé
    de récéption à : ISEP - 10 rue de Vanves Issy-les-Moulineaux
</p>



</body>
