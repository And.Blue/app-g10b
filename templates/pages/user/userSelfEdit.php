<?php $title = 'Modifier' .' '.$templateData['name'].' '.$templateData['surname'];?>



<h1>Modifier <?=$templateData['name'].' '.$templateData['surname']?></h1>
<?php
echo isset($message) ? '<div class="isa_success"><i class="fa fa-check"></i>'.$message.'</div>' : '';
?>
<div  class="selfInfos">
    <form action="" method="post">
        <label for="surname">Prénom</label>
        <input id="surname" type="text" name="surname" placeholder="Prénom" value="<?=$templateData['surname']?>">
        <label for="name">Nom</label>
        <input id="name" type="text" name="name" value="<?=$templateData['name']?>" placeholder="Nom">
        <label for="address">Adresse</label>
        <input id="address" type="text" name="address" placeholder="Adresse" value="<?=$templateData['address']?>">
        <label for="city">Ville</label>
        <input id="city" type="text" name="city" value="<?=$templateData['city']?>"placeholder="Ville">
        <label for="postal">Code Postal</label>
        <input id="postal" type="text" name="postal" value="<?=$templateData['postal']?>" placeholder="Code Postal">
        <label for="email">Email</label>
        <input id="email" type="text" name="email" value="<?=$templateData['email']?>" placeholder="Email">
        <label for="phone">Téléphone</label>
        <input id="phone" type="text" name="phone" value="<?=$templateData['phone']?>" placeholder="numéro de téléphone">
        <?php
        $role=$templateData['role'];
        ?>
        <label for="role">Role (non modifiable)</label>
        <select id="role" name="role">
            <option value="ROLE_USER" >Client</option>
        </select><br>

        <button class="" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span></button>
    </form>
</div>