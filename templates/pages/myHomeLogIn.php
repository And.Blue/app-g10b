<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 10/04/2019
 * Time: 23:05
 */

$title='Chez Moi';
$templateData['valeurUn']=45;
?>
<style>

    h2{
        font-family: Roboto;
        font-size: 35px;
        margin-left: 450px;
        font-color : #7c7a83;
    }

    #gauge{
        padding-left:700px ;
        margin-top: -150px;
    }

    #Wifi
    {
        margin-top: 60px;
    }
    #sensor1
    {

        margin-top: 120px;
    }
    #gauge_1{
        padding-left:700px ;
        margin-top: -170px;
    }

    #sensor2
    {
        margin-top: 140px;
    }
    .buttonEdit {
        display: inline-block;
        padding-left: 15px 25px;
        font-size: 45px;
        cursor: pointer;
        text-align: center;
        text-decoration: none;
        outline: none;
        color: #fff;
        background-color: #1387af;
        border: none;
        box-shadow: 0 9px #999;
        margin-left: 700px;
    }
    .buttonEdit {border-radius: 50%;}
    .buttonEdit:hover {background-color: #3e8e41}

    .buttonEdit:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    .body{

        background-size: cover;
    }
    #Edit{
        font-family: Roboto;
        font-size: 30px;
        margin-left: 215px;
    }


    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }



    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }



    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .onoff{

        margin-left: 58%;
        margin-top : -7%;
    }
    .bu{

        margin-top: 8%;
    }
    #wifi,#taka,#tata2
    {
        margin-right: 95%;
    }
</style>
 <div class ="myHome">
        <h2>Bienvenue chez vous </h2>
        <div class="">
            <img src="public/img/wifi.png" alt="wifi" style="max-width: 50px" id="Wifi">
            <p id="wifi"> Wi-fi </p>

            <script src="justgage-1.2.2/raphael-2.1.4.min.js"></script>
            <script src="justgage-1.2.2/justgage.js"></script>

            <div id="lala" class="200x160px" style = "max-width: 145px"></div>
            <script>
                var g = new JustGage({
                    id: "lala",
            <script src="public/js/justgage-1.2.2/raphael-2.1.4.min.js"></script>
            <script src="public/js/justgage-1.2.2/justgage.js"></script>

            <div id="gauge" class="200x160px" style = "max-width: 145px"></div>
            <script>
                var g = new JustGage({
                    id: "gauge",
                    value: getRandomInt(0,100),
                    min: 0,
                    max: 100,
                    title: "Wi-fi",
                    gaugeWidthScale : 0.2,
                    Color : "#9999ff"

                });
            </script>


        </div>

            <div class="">

            <img src="public/img/sensor.png" alt="sensor1" style="max-width: 50px" id="sensor1">
            <p id="taka">Sensor  </p>
                <div id="gauge_1" class="200x160px" style="max-width:145px "></div>
                    <script>
                        var g = new JustGage({
                            id: "gauge_1",
                            value: <?=$templateData['valeurUn']?> ,
                            min: 0,
                            max: 100,
                            title: "Sensor 1  ",
                            gaugeWidthScale : 0.2,
                            Color : "#9999ff"
                            });
                    </script>
        </div>

        <div class="">

            <img src="public/img/windows_icon.png" alt="windows" style ="max-width: 50px" id="sensor2">
            <p id="tata2"> Actionneur </p>
            <div class="onoff">
                 <label class="switch">
                    <input type="checkbox">
                    <span class="slider round"></span>
                </label>
            </div>


        </div>


            <div class="bu">
                    <button class ="buttonEdit" href="<?PATH?>home_sensor_add">+</button>
                <p id="Edit">Edit</p>
            </div>
    </div>

