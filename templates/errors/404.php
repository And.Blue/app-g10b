<?php $title='404 error'; ?>
<style scoped>
    #stripes3 {
        background-image: repeating-linear-gradient(180deg, #ccc, #ccc 30px, #dbdbdb 30px, #dbdbdb 60px);
    }
    .pattern {
        min-height: 200px;
        height: 100%;
    }

    .top {
        align-self: center;

    }
</style>

<div id='stripes3' class="pattern top">

    <div class="wrapper">
        <div class="container">
                        <h1>404</h1>
                        <div class=""><em><?=$_SERVER['REQUEST_URI']?></em> n'existe, Dommage la tortue n'est pas contente.</div>
                        <a href="/" class="btn btn-link">Retour à la maison</a>
                        <img class="" src="public/img/error_404.jpg" alt="Card image cap">
        </div>
    </div>
</div>



