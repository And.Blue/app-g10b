<style>
    .flex-container {
        margin: 10px;
        display: flex;
        justify-content: center;
        background-color: #1387af;;
        border-radius: 50px;
        box-shadow: 0px 1px 10px #9999;
    }

    .flex-container div {
        color: white;
        text-align: center;
        font-size: 20px;
        box-shadow: none;
    }

    .flex-container a {
        text-decoration: none;
        color: inherit;
        padding: 20px;
        box-shadow: transparent;
    }

    .flex-container a:hover {
        border-radius: 40px;
        background-color: #3c9cc1;
    }


</style>

<?php
$route = str_replace(PATH, '','/'.$_SERVER['REQUEST_URI']);
$route = explode('?', $route)[0];


$disabledRoutes = array(
        '/home',
    '/cond_utilisations',
    '/info',
    '/user/selfEdit',
    '/mentions-legales'
);


if (!in_array($route,$disabledRoutes)) {
    ?>
    <div class="flex-container">
        <div class="flex-container">
            <a href="<?=PATH?>announce">Annonces</a>
            <a href="<?=PATH?>my-house">Chez Moi</a>
            <a href="<?=PATH?>messages">Mes Messages</a>

<!--            <div><a href="--><?//=PATH?><!--announce"></a></div>-->
<!--            <div><a href="--><?//=PATH?><!--announce">Annonces</a></div>-->
        </div>

    </div>


    <?php
}
