
<div id="modal-register" class="modal topnav">
    <div class="modal-content">
        <span id="close-button-register" class="close-button">×</span>
        <h1>Inscription</h1>
        <div class="card">
            <div class="" id="registerStatus"></div>
            <div class="">
                <label for="surname">Prénom</label>
                <input id="surname" type="text" name="surname" placeholder="Surname">
                <label for="name">Nom</label>
                <input id="name" type="text" name="name" placeholder="Name">
                <button id="btn-register"  class="" style="vertical-align:middle; color: white"><span>Create</span></button>
            </div>
        </div>
    </div>
</div>