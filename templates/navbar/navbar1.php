<style>
    body {
        margin: 0;
        font-family: Roboto, sans-serif;
    }

    .btn {
        float: right;
        padding: 10px 10px;

    }

    .logo-image {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        overflow: hidden;
        /*margin-top: -6px;*/
        background: transparent;
        margin: -3px 140px -30px -70px;


    }

    .logo-image a:hover {
        background: transparent;
    }

    .topnav {
        overflow: hidden;
        background-color: #E4E7E8;
        border-radius: 2px;
        box-shadow: 0px 1px 10px #999999;
    }

    .topnav2 {
        overflow: hidden;
        /*background-color: #E4E7E8;*/
        border-radius: 2px;
        box-shadow: none;
        padding: 10px 90px;
        float: right;
        height: 28px;
        margin: -50px -60px 10px 20px;
        color: #dddddd;
        width: 150px;
    }


    .topnav a {
        float: left;
        display: block;
        color: #707070;
        text-align: left;
        padding: 14px 95px;
        text-decoration: none;
        font-size: 15px;
        font-weight: lighter;
        height: 28px;

    }


    .topnav2 a {
        float: right;
        display: block;
        color: #A8A8A8;
        text-align: left;
        padding: 5px 8px;
        text-decoration: none;
        font-size: 14px;
        font-weight: lighter;
        height: 15px;

    }


    .noHover {
        pointer-events: none;
    }

    .roundbox {
        border-radius: 12px;
        text-decoration: none;
        text-decoration-color: #707070;
        color: #707070;

    }


    .topnav a:hover {
        background-color: #dddddd;
        color: black;

    }

    .topnav2 a:hover {
        color: black;
        background-color: #dddddd

    }

    .newHover {
        margin: 0 0 0 0;
        border-radius: 20px;
        text-decoration: none;
        text-decoration-color: #707070;
        color: rgba(115, 110, 95, 0.56);

    .newHover a:hover {
        background-color: #dddddd;
    }

    .active {
        background-color: #4CAF50;
        color: white;

        /*<a href="




    }







    <?=PATH?>       register">S'inscrire</a>
    <a href="






    <?=PATH?>       register">Se connecter</a> */
    }

    .topnav .icon {
        display: none;
    }

    .teal {
        background: #009688;
    }


    @media screen and (max-width: 600px) {
        .topnav a:not(:first-child) {
            display: none;
        }

        .topnav a.icon {
            float: right;
            display: block;
        }
    }

    @media screen and (max-width: 600px) {
        .topnav.responsive {
            position: relative;
        }

        .topnav.responsive .icon {
            position: absolute;
            right: 0;
            top: 0;
        }

        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }
    }

    /*.logout {*/
    /*    visibility: visible;*/
    /*    position: fixed;*/
    /*    top: 10%;*/
    /*    right: 2%;*/
    /*    z-index: 1;*/
    /*    background-color: #E4E7E8;*/
    /*}*/
</style>

<div class="topnav " id="myTopnav">
    <a class="btn noHover" href="https://gitlab.com/And.Blue/app-g10b">
        <div class="logo-image">
            <img src="<?= PATH ?>public/img/domhouse_s.png" class="img-fluid" class="btn noHover" style="width: 100%">
        </div>
    </a>
    <a href="<?= PATH ?>home" class="roundbox">ACCUEIL</a>
    <?= isset($_SESSION['role']) ? '<a href="' . PATH . 'my-house" class="roundbox">CHEZ MOI</a>' : '' ?>
    <?= isset($_SESSION['role']) ? '<a href="' . PATH . 'info" class="roundbox">INFORMATIONS</a>' : '' ?>

    <!--    <a href="javascript:void(0);" class="icon" onclick="myFunction()">-->
    <!--        <i class="fa fa-bars"></i>-->
    <!--    </a>-->
</div>


<div class="topnav2" id="myTopnav2">

    <?php

    if (isset($_SESSION['logged']) && $_SESSION['logged']) {
        $name = $_SESSION['user']['name'];
        $surname = $_SESSION['user']['surname'];
        ?>
        <a id="login-link" class="newHover" href="<?=PATH?>logout">Se déconnecter</a>


        <?php
    } else {
        ?>
        <div class="" style="display: flex;flex-direction: row-reverse;margin-right: auto">
            <a id="login-link" href="<?= PATH ?>login" class="newHover">Login</a>
            <a style="color: #cccccc" class="noHover">|</a>
            <a id="register-link" href="<?= PATH ?>register" class="newHover">S'inscrire</a>
        </div>
        <?php
    }
    ?>
</div>

<script>

    const print = (...args) => console.log(...args);

    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

</script>