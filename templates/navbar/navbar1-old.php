<style>
    body {
        margin: 0;
        font-family: Roboto, sans-serif;
    }


    .logo-image{
        width: 32px;
        height: 32px;
        border-radius: 50%;
        overflow: hidden;
        /*margin-top: -6px;*/
        background: transparent;

    }

    .logo-image a:hover{
        background: transparent;
    }

    .topnav {
        overflow: hidden;
        background-color: #E4E7E8;
        border-radius: 2px;
        box-shadow: 0px 1px 10px #999999;


    }

    .topnav a {
        float: left;
        display: block;
        color: #707070;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 20px;
        font-weight: bolder;
        height : 28px;

    }

    .modal {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        opacity: 0;
        visibility: hidden;
        transform: scale(1.1);
        transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
    }

    .modal-content {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 1rem 1.5rem;
        width: 24rem;
        border-radius: 0.5rem;
    }

    .close-button {
        float: right;
        width: 1.5rem;
        line-height: 1.5rem;
        text-align: center;
        cursor: pointer;
        border-radius: 0.25rem;
        background-color: transparent;
    }

    .close-button:hover {
        background-color: darkgray;
    }

    .show-modal {
        opacity: 1;
        visibility: visible;
        transform: scale(1.0);
        transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
    }


    .noHover{
        pointer-events: none;
    }

    .roundbox{
        border-radius: 12px;
    }


    .topnav a:hover {
        background-color: #dddddd;
        color: black;

    }


    .active {
        background-color: #4CAF50;
        color: white;
    }

    .topnav .icon {
        display: none;
    }

    .teal
    {
        background: #009688;
    }


    @media screen and (max-width: 600px) {
        .topnav a:not(:first-child) {display: none;}
        .topnav a.icon {
            float: right;
            display: block;
        }
    }

    @media screen and (max-width: 600px) {
        .topnav.responsive {position: relative;}
        .topnav.responsive .icon {
            position: absolute;
            right: 0;
            top: 0;
        }
        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }
    }
</style>

<div class="topnav " id="myTopnav">
    <a class="btn noHover" href="https://gitlab.com/And.Blue/app-g10b">
        <div class="logo-image">
            <img src="<?=PATH?>public/img/domhouse_s.png" class="img-fluid" class="btn noHover" style="width: 100%">
        </div>
    </a>
    <a href="<?=PATH?>homeTest" class="roundbox">Home</a>
    <a href="<?=PATH?>register" class="roundbox">Register</a>
    <a href="<?=PATH?>home" class="roundbox">Home</a>
    <a href="<?=PATH?>users" class="roundbox">Users</a>
    <a class="roundbox" id='trigger-login'>Login</a>
    <a class="roundbox" id='trigger-register'>S'inscrire</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>


    </a>
</div>
    <!--    MODAL LOGIN AND REGISTER   -->
    <?php require 'templates/navbar/loginModal.php';?>

    <!--    Modal that Pops-up with User registration-->
    <?php require 'templates/navbar/registerModal.php';?>


<!--This is the script for all the Login and Register Modals-->

<script type="module" src="public/js/ajax/modalCall.js"></script>
<script type="module" src="../../public/js/old/navbarModals.js"></script>


<!--This makes the navbar responsive (hamburger appears when window's width is scaled down-->
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>