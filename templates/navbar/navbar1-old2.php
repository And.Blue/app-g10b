<style>
    body {
        margin: 0;
        font-family: Roboto, sans-serif;
    }

    .btn {
        float: right;
        padding: 10px 10px;

    }

    .logo-image {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        overflow: hidden;
        /*margin-top: -6px;*/
        background: transparent;
        margin: -3px 140px -30px -70px;


    }

    .logo-image a:hover {
        background: transparent;
    }

    .topnav {
        overflow: hidden;
        background-color: #E4E7E8;
        border-radius: 2px;
        box-shadow: 0px 1px 10px #999999;


    }

    .topnav2 {
        overflow: hidden;
        background-color: #E4E7E8;
        border-radius: 2px;
        box-shadow: none;
        padding: 10px 50px;
        float: right;
        height: 28px;
        margin: -50px 20px 20px 20px;
        color: #707070;

    }


    .topnav a {
        float: left;
        display: block;
        color: #707070;
        text-align: left;
        padding: 14px 95px;
        text-decoration: none;
        font-size: 20px;
        font-weight: lighter;
        height: 28px;

    }


    .topnav2 b {
        float: right;
        display: block;
        color: #000;
        text-align: right;
        padding: 10px 50px;
        text-decoration: none;
        font-size: 8px;
        font-weight: lighter;
        height: 8px;

    }


    .noHover {
        pointer-events: none;
    }

    .roundbox {
        border-radius: 12px;
        text-decoration: none;
        text-decoration-color: #707070;
        color: #BDBDBD;

    }


    .topnav a:hover {
        background-color: #dddddd;
        color: black;

    }

    .topnav2 b:hover {
        background-color: transparent;
        color: transparent;

    }


    .active {
        background-color: #4CAF50;
        color: white;

        /*<a href="

    <?=PATH?>  register">S'inscrire</a>
    <a href="

    <?=PATH?>  register">Se connecter</a> */
    }

    .topnav .icon {
        display: none;
    }

    .teal {
        background: #009688;
    }


    @media screen and (max-width: 600px) {
        .topnav a:not(:first-child) {
            display: none;
        }

        .topnav a.icon {
            float: right;
            display: block;
        }
    }

    @media screen and (max-width: 600px) {
        .topnav.responsive {
            position: relative;
        }

        .topnav.responsive .icon {
            position: absolute;
            right: 0;
            top: 0;
        }

        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }
    }
</style>
<div class="topnav " id="myTopnav">
    <a class="btn noHover" href="https://gitlab.com/And.Blue/app-g10b">
        <div class="logo-image">
            <img src="<?= PATH ?>public/img/domhouse_s.png" class="img-fluid" class="btn noHover" style="width: 100%">
        </div>
    </a>
    <a href="<?= PATH ?>home" class="roundbox">ACCUEIL</a>
    <a href="<?= PATH ?>myHome" class="roundbox">CHEZ MOI</a>
    <a href="<?= PATH ?>info" class="roundbox">INFORMATIONS</a>


    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>

    </a>
</div>

<div class="topnav2" id="myTopnav2">
    <?php
    if (isset($_SESSION['status']) && $_SESSION['status'] == 'loggedin') {
        ?>
        <a href="<?= PATH ?>login" class="roundbox">Bonjour <?= $_SESSION['user']['name'].' '.$_SESSION['user']['surname']?> | </a>
        <a href="<?= PATH ?>logout" class="roundbox">Logout</a>
        <?php
    } else {
        ?>
        <a href="<?= PATH ?>login" class="roundbox">Login | </a>
        <a href="<?= PATH ?>register" class="roundbox">S'inscrire</a>
        <?php
    }
    ?>


</div>


<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>