<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        body {
            margin: 0;
            font-family: "Lato", sans-serif;
            height: 100%;
        }


        .sidebar {
            margin: 0;
            padding: 0;
            width: 220px;
            background-color: #f1f1f1;
            position: absolute;
            height: 110%;
            overflow: hidden;
            border-radius: 2px;
            box-shadow: 0px 1px 10px #999999;

        }


        .sidebar a {
            display: block;
            position: relative;
            color: black;
            padding: 16px;
            text-decoration: none;
        }

        .sidebar .submenu a{
            padding: 10px;

        }
        .sidebar li {
            list-style-type: none;
            position: relative;
            left: -20px;
            font-size: 90%;
        }

        .sidebar div{
            color: black;
            padding: 16px;
            position: fixed;


        }

        .sidebar span{
            display:block;
            position: relative;
            color: black;
            padding: 16px;
            text-decoration: none;
            cursor: pointer;
        }


        .sidebar a:hover, .sidebar span:hover{
            background-color: gray;
            border-radius: 0px 15px 15px 0px;
            color: white;
        }

        .submenu {
            display: none;
            position: relative;

        }

        .submenu li {
            list-style-type: none;
        }


        div.container {
            margin-left: 250px;
            padding: 1px 16px;

            min-height: 100%;
            position: relative;
        }



        .sidebar {
            position: fixed;
        }

        @media screen and (max-width: 700px) {
            .sidebar {
                width: 100%;
                height: auto;
                position: relative;
            }

            .sidebar a {
                float: left;
            }

            div.container {
                margin-left: 0;
            }
        }

        @media screen and (max-width: 400px) {
            .sidebar a {
                text-align: center;
                float: none;
            }
        }
    </style>
</head>
<body>

<?php

require 'templates/Admin/sidepanel/sidepanel.php'; ?>

<div class="container">
    <?php echo $content;
    ?>
</div>




</body>
</html>