<?php $title = 'Ajout d\'une habitation'; ?>

<h1>Ajout d'une habitation</h1>


<?php
echo isset($templateData['message']) ? '<div class="isa_warning container"><i class="fa fa-warning"></i>' . $templateData['message'] . '</div>' : '';
?>
<div class="card">

    <form method="post">
        <label for="name">Nom de l'habitation</label>
        <input type="text" id="name" name="name" placeholder="Veuillez rentrer le nom choisi pour l'habitation">
        <label for="description">Description de l'habitation (optionel)</label>
        <input type="text" id="description" name="description" placeholder="Ce qui décrit au mieux votre habitation">
        <button type="submit">Créer une habitation</button>
    </form>

</div>



