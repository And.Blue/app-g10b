<?php $title = "Validation Utilisateur"; ?>

<h2>Validation d'utilisateur</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores doloribus, officia. Ab itaque neque nostrum placeat
    soluta. Ab amet animi, consectetur ducimus esse, iure non optio quibusdam soluta vero voluptatum?</p>
<?php
$thread = $templateData;
?>

<h4>Numéro utilisateur: <?= $thread['id_user'] ?></h4>
<h4>Nom: <?= $thread['name'] ?></h4>
<h4>Prénom: <?= $thread['surname'] ?></h4>
<h4>Email: <?= $thread['email'] ?></h4>
<h4>Téléphone: <?= $thread['phone'] ?></h4>
<h4>Role: <?= $thread['role'] ?></h4>
<h4>Date de création: <?= $thread['create_at'] ?></h4>
<h4>Adresse: <?= $thread['address'] ?> Ville: <?= $thread['city'] ?> Code Postal: <?= $thread['postal'] ?></h4>

<h2>Actions sur l'utilisateur </h2>
<a class="button red" href="<?= PATH . 'delete_user?' . $thread['id_user'] .'&type=validation'?>"><span class="x-anim">Supprimer l'utilisateur</span></a>
<a class="button green" href="<?= PATH . 'activate_user?' . $thread['id_user'] ?>"><span>Valider l'utilisateur</span></a>
