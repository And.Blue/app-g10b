<?php $title = 'Comptes en Attende d\'activation'; ?>

    <h1>Comptes en attente d'activation</h1>


    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut blanditiis deserunt dolore doloribus ea
        earum
        et explicabo, harum magnam minus, quae quisquam, reiciendis similique voluptates. Enim mollitia quis
        reprehenderit.</p>


<?php
if (sizeof($templateData) !== 0) {
    ?>

    <input type="text" id="searchInput" onkeyup="applySearch()" placeholder="Rechercher parmis les comptes en attente d'activation par Nom">
    <div id="test"></div>

    <table id="dynamicTable"><tr>
        <th>#</th>
        <th>Email</th>
        <th>Rôle</th>
        <th>Date de Création</th>
        <th>Actions</th>
    </tr>
    <?php
    foreach ($templateData

             as $user) {
        ?>
        <tr class="hover-row">
        <?php echo '<td>' . $user['id_user'] . '</td><td>' . $user['email'] . '</td><td>' . $user['role'] . '</td><td>' . $user['create_at']
            . '</td><td><a class="button red" href="' . PATH . 'delete_user?' . $user['id_user'] . '&type=validation"><span class="x-anim">Supprimer</span></a><a class="button" href="' . PATH . 'user/see?' . $user['id_user'] . '"><span class="x-anim">Voir et valider</span></a>
</td>
</tr>';
    }
}
?>
    </table>
<?php if (sizeof($templateData) === 0) {
    echo '<tr><td style="background-color: transparent">pas d\'utilisateurs en attente de validation...</td></tr>';
}

?>
<script>

    function applySearch() {
        console.log('executed');
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("dynamicTable");
        tr = table.getElementsByTagName("tr");
        for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }

</script>
