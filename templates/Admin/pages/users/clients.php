<?php $title = 'Clients'; ?>

<h1>Liste des Clients</h1>
<p style="color: red; font-weight: bold">Seuls les utilisateurs ici sont ceux qui ont été activés</p>


<input type="text" id="searchInput" onkeyup="applySearch()" placeholder="Rechercher parmis les Clients par Nom">
<div id="test"></div>

<table id="dynamicTable">
    <tr>
        <th>#</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Email</th>
        <th>Téléphone</th>
        <th>Date de Création</th>
        <th>Actions</th>
    </tr>
    <?php
    if (sizeof($templateData) !== 0)
    {
    foreach ($templateData as $user) {
        echo '<tr><td>' . $user['id_user'] . '</td><td>' . $user['name'] . '</td><td>' . $user['surname'] . '</td><td>' . $user['email'] . '</td><td>' . $user['phone'] . '</td><td>' . $user['create_at']
            . '</td><td><a class="button red" href="' . PATH . 'delete_user?' . $user['id_user'] . '&type=client"><span class="x-anim">Delete</span></a><a class="button" href="' . PATH . 'user/edit?' . $user['id_user'] . '"><span class="">Editer</span></a>
</td>
</tr>';
    }
    ?>
</table>
<?php

}
else {
    echo 'Pas de clients dans la base de données';
}

?>
<script>

    function applySearch() {
        console.log('executed');
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("dynamicTable");
        tr = table.getElementsByTagName("tr");
        for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }

</script>
