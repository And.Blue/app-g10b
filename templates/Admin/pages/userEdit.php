<?php
$homes = $templateData['all_homes'];
$templateData = $templateData['user_info'];
$title = 'Modifier ' . $templateData['name'] . ' ' . $templateData['surname'];
?>


<h1>Modifier <?= $templateData['name'] . ' ' . $templateData['surname'] ?></h1>

<a style="text-decoration: none" href="<?= PATH ?>admin/users/clients"><i class="fa fa-chevron-circle-left"></i>Retour à
    la liste des utilisateurs</a>

<?php
echo isset($message) ? '<div class="isa_success"><i class="fa fa-check"></i>' . $message . '</div>' : '';
?>

<div id="dynamicCard">
    <div class="card-container">
        <div class="card-big u-clearfix">
            <div class="card-body">
                <form action="" method="post">
                    <label for="surname">Prénom</label>
                    <input id="surname" type="text" name="surname" placeholder="Prénom"
                           value="<?= $templateData['surname'] ?>">
                    <label for="name">Nom</label>
                    <input id="name" type="text" name="name" value="<?= $templateData['name'] ?>" placeholder="Nom">
                    <label for="address">Adresse</label>
                    <input id="address" type="text" name="address" placeholder="Adresse"
                           value="<?= $templateData['address'] ?>">
                    <label for="city">Ville</label>
                    <input id="city" type="text" name="city" value="<?= $templateData['city'] ?>" placeholder="Ville">
                    <label for="postal">Code Postal</label>
                    <input id="postal" type="text" name="postal" value="<?= $templateData['postal'] ?>"
                           placeholder="Code Postal">
                    <label for="email">Email</label>
                    <input id="email" type="text" name="email" value="<?= $templateData['email'] ?>"
                           placeholder="Email"><br>
                    <label for="phone">N° de Téléphone</label>
                    <input id="phone" type="text" name="phone" value="<?= $templateData['phone'] ?>"
                           placeholder="numéro de téléphone">
                    <label for="role">Rôle</label>

                    <?php
                    $role = $templateData['role'];
                    ?>

                    <select id="role" name="role">
                        <option value="ROLE_USER" <?php if ($role == 'ROLE_USER') echo 'selected'; ?>>Client</option>
                        <option value="ROLE_ADMIN" <?php if ($role == 'ROLE_ADMIN') echo 'selected'; ?>>Admin</option>
                    </select>

                    <button class="" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span>
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>
<!---->
<!--<h2>Habitations</h2>-->
<!---->
<!--<div id="dynamicCard">-->
<!--    --><?php
//    $count = 1;
//    foreach ($homes as $home) {
//        echo '<div class="card-container">
//        <div class="card-big u-clearfix">
//            <div class="card-body">
//                <span class="card-number card-circle subtle">' . $count . '</span>
//                <h2 class="card-title">' . $home['name'] . '</h2>
//                <span class="card-author subtle"><em>' . $home['description'] . ' ' . $home['name'] . ' - ajouté le ' . $home['create_at'] . '</em></span>
//                <span class="card-description subtle">The items sensors will be there one day [to add].</span>
//                <div class="card-read"></div>
//                <a class="button" href=""><span>modérer [todo]</span></a>
//                <a class="button red" onclick="confirm(`Voulez-vous supprimer cette maison ?`);" href="' . PATH . 'house_delete?id_house=' . $home['id_house'] . '&id_user=' . $home['id_user'] . '"><span class="x-anim">Supprimer</span></a>
//<!--                <span class="card-tag card-circle subtle">C</span>-->
//            </div>
//        </div>
//        <div class="card-shadow"></div>
//    </div>
//
//';
//        $count++;
//    }
//    ?>
<!--</div>-->
<!--<a href="--><?//= PATH ?><!--admin/user/home/create?--><?//= $templateData['id_user'] ?><!--"-->
<!--   class="button"><span>Ajouter une habitation</span></a>-->
<!---->
