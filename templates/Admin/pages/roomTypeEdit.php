<?php
$title = 'Modifier Type : '.$templateData['name'] ; ?>


<h1>Modification Type Pièce</h1>


<div class="form-window">
    <form action="" method="post">
        <label for="room-type"><br />Type de Pièce</label><br />
        <input id="room-type" type="text" name="room-type" value="<?=$templateData['name']?>"placeholder="Ex : Cuisine, Salle de bain..."><br /><br>
        <label for="room-description">Description</label><br />
        <input id="room-description" type="text" value="<?=$templateData['description']?>" name="room-description" placeholder="Description du Type de pièce (optionnel)">
        <button id="modifier" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span></button>
        <label for="modifier"></label>
    </form>
    <a class="button red" href="<?=PATH?>admin/offer/room-type"><span class="">Annuler</span></a>
</div>



