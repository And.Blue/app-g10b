<?php $title = 'Modification d\'annonce'; ?>
<h1>Modification d'annonce</h1>

<div>
    <a href="<?=PATH?>admin/information/all">Revenir à toutes les annonces</a>
</div>


<div id="dynamicCard">
    <div class="card-container">
        <div class="card-big u-clearfix">
            <div class="card-body">
                <form method="post">
                    <h2 class="card-title">
                        <label for="title">Titre</label>
                        <input type="text" id="title" name="title" value="<?=$templateData['title']?>" placeholder="Titre de l'annonce"/>
                        <label for="text">Contenu de l'annonce</label>
                        <textarea id="text" name="text" rows="5" cols="20" placeholder="Veuillez ajouter votre annonce"><?=$templateData['text']?></textarea></span>
                    </h2>
                    <span class="card-author subtle"><em>Crée par <?= $_SESSION['user']['name'] . ' ' . $_SESSION['user']['surname'] ?></em></span>

                    <div class="card-read"></div>
                    <button class="button green" style="float: right;" type="submit">Modifier</button>
                </form>

            </div>
        </div>
        <div class="card-shadow"></div>
    </div>
