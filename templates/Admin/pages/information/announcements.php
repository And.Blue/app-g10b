<?php $title = 'Annonces Générales'; ?>
<h1>Annonces générales</h1>
<p>Ci-dessous la liste de toutes les annonces générales préalablement créees </p>


<input type="text" id="searchInput" onkeyup="applySearch()" placeholder="Rechercher parmis les annonces par titre">

<div id="dynamicCard">
    <?php
    foreach ($templateData as $info) {
        echo '<div class="card-container">
        <div class="card-big u-clearfix">
            <div class="card-body">
                <span class="card-number card-circle subtle">' . $info['id_announcement'] . '</span>
                <h2 class="card-title">' . $info['title'] . '</h2>
                <span class="card-author subtle"><em>' . $info['surname'] . ' ' . $info['name'] . ' - modifié le ' . $info['modif_at'] . '</em></span>
                <span class="card-description subtle">' . $info['text'] . '</span>
                <div class="card-read"></div>
                <a class="button" href="' . PATH . 'admin/information/modify?' . $info['id_announcement'] . '"><span>Modifier</span></a>
                <a class="button red" href="' . PATH . 'information_delete?' . $info['id_announcement'] . '"><span class="x-anim">Supprimer</span></a>
<!--                <span class="card-tag card-circle subtle">C</span>-->
            </div>
        </div>
        <div class="card-shadow"></div>
    </div>

';


    }
    ?>
</div>

<a class="button-add" href="<?= PATH ?>admin/information/create">+</a>

<script>

    function applySearch() {
        console.log('executed');
        var input, filter, cardList, cards, title, i, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        cardList = document.getElementById("dynamicCard");
        cards = cardList.getElementsByClassName("card-container");
        for (i = 0; i < cards.length; i++) {
            title = cards[i].getElementsByClassName("card-title")[0];
            console.log('title', title.textContent);
            txtValue = title.textContent || title.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                cards[i].style.display = "";
            } else {
                cards[i].style.display = "none";
            }
        }
    }

</script>
