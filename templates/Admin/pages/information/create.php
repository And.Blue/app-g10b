<?php $title = 'Nouvelle Annonce'; ?>
<h1>Nouvelle Annonce générale</h1>

<a style="text-decoration: none" href="<?=PATH?>admin/information/all"><i class="fa fa-chevron-circle-left"></i>Retour à toutes les annonces</a>

<div id="dynamicCard">
    <div class="card-container">
        <div class="card-big u-clearfix">
            <div class="card-body">
                <form method="post">
                    <h2 class="card-title">
                        <label for="title">Titre</label>
                        <input type="text" id="title" name="title" placeholder="Titre de l'annonce"/>
                        <label for="text">Contenu de l'annonce</label>
                        <textarea id="text" name="text" rows="5" cols="20" placeholder="Veuillez ajouter votre annonce"></textarea></span>
                    </h2>
                    <span class="card-author subtle"><em>Crée par <?= $_SESSION['user']['name'] . ' ' . $_SESSION['user']['surname'] ?></em></span>

                    <div class="card-read"></div>
                    <button class="button green" style="float: right;" type="submit">Créer</button>
                </form>
            </div>
        </div>
        <div class="card-shadow"></div>
    </div>
</div>
