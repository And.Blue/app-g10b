<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 15/04/2019
 * Time: 18:11
 */
?>
<h1>Modification capteurs</h1>


<div class="form-window">
        <form action="" method="post">
            <label for="name"><br />Nom</label><br />
            <input id="name" type="text" name="name" placeholder="Sens1" value="<?=$templateData['name']?>"><br /><br>
            <label for="type_compo">Type</label><br/>
            <select id="type_compo" name="type_compo" value="<?=$templateData['type_compo']?>">
                <option value="CAPT_VALUE">Capteur</option>
                <option value="ACT_VALUE">Actionneur</option>
            </select><br>
            <label for="reference">Reference</label><br/>
            <input id="reference" type="text" name="reference" value="<?=$templateData['reference']?>" placeholder="T4850,A7850">
            <label for="user">User</label><br/>
            <input id="user" type="int" name="user" value="<?=$templateData['user']?>" placeholder="14,15,85">
        <button id="modifier" type="submit" style="vertical-align:middle; color: white"><span>Modifier</span></button>
        <label for="modifier"></label>
    </form>
    <a class="button red" href="<?=PATH?>admin/offer/sensor-type"><span class="">Annuler</span></a>
</div>

