<?php $title = 'Toutes les conversations'; ?>

<h1>Toutes les conversations</h1>

<style>
    .fa-trash:hover {
        color: white;
        background: darkred;
        border-radius: 50px;
        padding: 2px;
        cursor: pointer;
    }
</style>

<input type="text" id="searchInput" onkeyup="applySearch()" placeholder="Rechercher parmis les conversations">
<div id="test"></div>

<table id="dynamicTable">
    <tr>
        <th></th>
        <th>Titre</th>
        <th>Auteur</th>
        <th>Destinataire</th>
        <th>Actions</th>
    </tr>
    <?php


    '<span class="right-side" style="color: darkred"><a onclick="confirm(`Voulez-vous réelement supprimer ce message?`)" href="' . PATH . 'delete_message?' . $message['id_message'] . '" style="text-decoration: none;color: inherit"><i class="fa fa-trash"></a></i></span>';

    if (sizeof($templateData) !== 0)
    {
    foreach ($templateData as $thread) {
        echo '<tr><td><i class="fa fa-comments"></i></td><td>' . $thread['topic'] . '</td>
            <td>' . $thread["author_name"] . ' '.$thread['author_surname'].'</td><td>' . $thread['recipient_name'] . ' '.$thread['recipient_surname'].'</td>
            <td><a class="button red" style="margin-top: 0" onclick="confirm(`Voulez-vous réelement supprimer cette conversation ?`)" href="' . PATH . 'delete_thread?' . $thread['id_conv'] . '" 
            style="text-decoration: none;color: inherit;"><i class="fa fa-trash" style="font-size: 2rem"></i></a><a class="button" href="' . PATH . 'admin/thread/view?' . $thread['id_conv'] . '"><span class="">Voir les Messages</span></a>
</td>
</tr>';
    }
    ?>
</table>
<?php
}
else {
    echo 'Pas de conversations dans la base de données';
}
?>
<a class="button-add" href="<?= PATH ?>admin/thread/new">+</a>

<script>
    function applySearch() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("dynamicTable");
        tr = table.getElementsByTagName("tr");
        for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
</script>





