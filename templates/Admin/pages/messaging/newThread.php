<?php $title = 'Nouvelle conversation'; ?>

<link rel="stylesheet" href="<?= PATH ?>public/css/page/newThread.css">


<h1>Nouvelle conversation</h1>
<a style="text-decoration: none" href="<?= PATH ?>admin/thread/all"><i class="fa fa-chevron-circle-left"></i> Retour à
    la liste des conversations</a>


<div class="columnContact">
    <div class="cardContact">
        <div class="container">

            <form method="post">
                <br> <!-- A ne jamais faire !-->
                <label for="name">Sujet de la conversation</label>
                <input type="text" id="name" name="subject"
                       placeholder="Entez le sujet de la conversation">

                <label for="id_destinator">Destinataire</label>
                <select name="id_destinator" id="user">
                    <option value="---"> --- </option>
                </select>

                <label for="subject">Message</label>
                <textarea id="subject" name="message" placeholder="Ecrivez un premier message pour cette conversation..." style="height:200px"></textarea>
                <input type="submit" value="Submit">
            </form>
        </div>

    </div>
</div>


<script type="text/javascript" src="<?= PATH ?>public/js/api.js"></script>
<script>
    const select = document.getElementById('user');

    function addOption(select, option) {
        let optionNode = select.appendChild(
            document.createElement('option')
        );
        optionNode.appendChild(
            document.createTextNode(option.text)
        );
        optionNode.value = option.value;
    }


    get('/api/users').then(res => {
        res.data.map(user => {
                addOption(select, {text: `${user.name} ${user.surname}`, value: user.id_user});
            }
        );
    }).catch(err => console.log(err));


</script>


