<?php $title = 'Types de Capteurs';
?>

  <h1>Gestion des Types de Capteurs</h1>
    <h3>Types existants</h3>

<h3>Création d'un Type de capteur/actionneur</h3>


<div class="form-window">
    <form action="<?=PATH?>sensor_type_add" method="post">
        <label for="name"><br />Nom</label><br />
        <input id="name" type="text" name="name" placeholder="Sens1"><br /><br>
        <label for="type_compo">Type</label><br/>
            <select id="type_compo" name="type_compo">
                 <option value="CAPT_VALUE">Capteur</option>
                 <option value="ACT_VALUE">Actionneur</option>
              </select><br>
        <label for="reference">Reference</label><br/>
        <input id="reference" type="text" name="reference" placeholder="T4850,A7850">



              </select><br>
        <button class="" type="submit" style="vertical-align:middle; color: white"><span>+</span></button>
    </form>
</div>
<div class="form-window" >

    <table>
        <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Type</th>
            <th>References</th>

            <th>Actions</th>

        </tr>

       <?php

    if (sizeof($templateData) !== 0)
    {
        foreach ($templateData['sensorTypes'] as $sensorTypes) {

            echo '<tr>
                        <td>' . $sensorTypes['id_composant_type'] . '</td>
                        <td>' . $sensorTypes['name'] . '</td>
                        <td>' . $sensorTypes['type_compo'] . '</td>
                        <td>' . $sensorTypes['reference'] . '</td> 
                         <td><a class="button s red" href="' . PATH . 'sensor_type_delete?' . $sensorTypes['id_composant_type'] . '">
                            <span class="x-anim">Delete</span></a>
                            <a class="button s green" href="' . PATH . 'admin/offer/sensor-type/edit?' . $sensorTypes['id_composant_type'] . '">
                            <span class="">Editer</span></a>
                          </td>   
                         
</tr>';
        }
    ?>
        </table>
        <?php
    }
    else {
        echo 'no room types found in database';
    }

    ?>