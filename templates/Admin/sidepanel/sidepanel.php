<script>
    const unfold = (elem) => {
        const ul = elem.nextElementSibling;
        const icone = elem.firstChild;
        console.log(icone);
        if (ul.style.display === 'none' ||  ul.style.display === '') {
            ul.style.display = 'block';
            ul.style.transition
            icone.style.transform = 'rotate(90deg)';

        } else {
            ul.style.display = 'none';
            icone.style.transform = 'rotate(0deg)';

        }
    };
</script>

<div class="sidebar">
    <div style="padding: 20px">
        <i class="fa fa-user-circle fa-3x"></i>
        <?= $_SESSION['user']['name'] . ' ' . $_SESSION['user']['surname'] ?>
        <hr style="margin-top: 20px; width: 120%">
    </div>

      <div class="" style="margin-top: 90px">
          <span onclick="unfold(this)"><i class="fa fa-caret-right"></i> Annonces Générales</span>
          <ul class="submenu">
              <li><a href="<?= PATH ?>admin/information/all"><i class="fa fa-bullhorn"></i> Toutes les annonces</a></li>
              <li><a href="<?= PATH ?>admin/information/create"><i class="fa fa-plus-circle"></i> Créer une annonce</a></li>
          </ul>


          <span onclick="unfold(this)"><i class="fa fa-caret-right"></i> Messages</span>
          <ul class="submenu">
              <li><a href="<?= PATH ?>admin/thread/all"><i class="fa fa-inbox"></i>  Voir toutes les Conversations</a></li>
              <li><a href="<?= PATH ?>admin/thread/new"><i class="fa fa-envelope"></i> Ecrire un message</a></li>
          </ul>



          <span onclick="unfold(this)"><i class="fa fa-caret-right"></i> Utilisateurs</span>
          <ul class="submenu">
              <li><a href="<?= PATH ?>admin/users/all"><i class="fa fa-users"></i> Tous les Utilisateurs</a></li>
              <li><a href="<?= PATH ?>admin/users/clients"><i class="fa fa-user"></i> Clients</a></li>
              <li><a href="<?= PATH ?>admin/users/activate"><i class="fa fa-bell"></i> Comptes en attente d'activation</a></li>
          </ul>

          <span onclick="unfold(this)"><i class="fa fa-caret-right"></i> Gestion des Offres </span>
          <ul class="submenu">
              <li><a href="<?= PATH ?>admin/offer/sensor-type"><i class="fa fa-wrench"></i> Types de Capteurs ou Actionneurs</a></li>
              <li><a href="<?= PATH ?>admin/offer/room-type"><i class="fa fa-home"></i> Types de pièces</a></li>
          </ul>

          <a href="#contact"><i class="fa fa-comments"></i> Contact</a></li>
          <a href="#about"><i class="fa fa-info-circle"></i> About</a></li>
          <a href="<?= PATH ?>logout"><i class="fa fa-power-off"></i> Logout</a></li>
      </div>

</div>

