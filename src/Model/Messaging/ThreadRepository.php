<?php

namespace src\Model\Messaging;

use lib\DomHouseFramework\Model\AbstractRepository;

class ThreadRepository extends AbstractRepository
{

    public function getAll()
    {
        $req = $this->database->query(
            'SELECT c.id_conv, c.name as topic, u2.name as recipient_name, u2.surname as recipient_surname,
                        u1.name as author_name, u1.surname as author_surname FROM conversation AS c 
                        INNER JOIN user AS u1 ON c.id_user_1 = u1.id_user
                        INNER JOIN user AS u2 ON c.id_user_2 = u2.id_user');

        $data = $req->fetchAll(\PDO::FETCH_ASSOC);

        $req->closeCursor();


        return $data;
    }


    public function getAllByUserId($id)
    {
        $req = $this->database->query(
            'SELECT c.id_conv, c.name as topic, u2.name as recipient_name, u2.surname as recipient_surname,
                        u1.name as author_name, u1.surname as author_surname FROM conversation AS c 
                        INNER JOIN user AS u1 ON c.id_user_1 = u1.id_user
                        INNER JOIN user AS u2 ON c.id_user_2 = u2.id_user WHERE u1.id_user='.$id);

        $data = $req->fetchAll(\PDO::FETCH_ASSOC);

        $req->closeCursor();


        return $data;

    }

    public function add(array $thread)
    {
        $req = $this->database->prepare('INSERT INTO conversation(name,id_user_1, id_user_2) VALUES(?,?,?)');
        $req->execute(array($thread['name'], $thread['id_user_1'], $thread['id_user_2']));
        $req->closeCursor();

        $query = $this->database->query('SELECT LAST_INSERT_ID() AS id_conv');
        $data = $query->fetch(\PDO::FETCH_ASSOC);
        $query->closeCursor();
        return $data;
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM conversation WHERE id_conv=' . $id);
    }



    public function get($id)
    {
        $req = $this->database->query(
            'SELECT m .id_message, m.title as title,m.content, m.id_destinator as id_recipient,m.sent_at, u2.name as recipient_name, 
    u2.surname as recipient_surname, u1.name as author_name, u1.surname as author_surname FROM message AS m 
    INNER JOIN user AS u1 ON m.id_author = u1.id_user INNER JOIN user AS u2 ON m.id_destinator = u2.id_user
     WHERE m.id_conversation='.$id.' ORDER BY id_message DESC');

        $data = $req->fetchAll(\PDO::FETCH_ASSOC);

        $req->closeCursor();


        return $data;
    }


}
