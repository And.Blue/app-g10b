<?php

namespace src\Model\Messaging;

use lib\DomHouseFramework\Model\AbstractRepository;

class MessageRepository extends AbstractRepository
{


    public function deleteByConvId($id)
    {
        $req = $this->database->query('DELETE FROM message WHERE id_conversation=' . $id);
        $req->closeCursor();
    }

    public function deleteById($id)
    {
        $req = $this->database->query('DELETE FROM message WHERE id_message=' . $id);
        $req->closeCursor();
    }

    public function getById($id)
    {
        //Because we like Js sometimes
        return $this
            ->database
            ->query('SELECT * FROM message WHERE id_message=' . $id)
            ->fetch(\PDO::FETCH_ASSOC);
    }

    public function add(array $message)
    {
        $req = $this->database->prepare('INSERT INTO message(title,id_author,id_destinator, id_conversation,content) VALUES(?,?,?,?,?)');

        $req->execute(array($message['name'], $message['id_user_1'], $message['id_user_2'], $message['id_conv'], $message['content']));
        $req->closeCursor();
    }

}
