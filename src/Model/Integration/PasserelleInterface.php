<?php

namespace src\Model\Integration;

class PasserelleInterface
{
    private $url;
    private $trames;

    public function __construct($url)
    {
        $this->url = $url;
        $this->init();
    }

    private function init()
    {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            $this->url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $data = curl_exec($ch);
        curl_close($ch);

        $this->trames = array_reverse(str_split($data, 33));
    }


    public function readAllValues()
    {
        for ($i = 0; $i < sizeof($this->trames); $i++) {
            $this->read($i);
        }
    }

    public function getSingleValue($nb)
    {
        $trame = $this->trames[$nb];
        $t = substr($trame, 0, 1);

        list($t, $o, $r, $c, $n, $v, $a, $x, $year, $month, $day, $hour, $min, $sec) =
            sscanf($trame, "%1s%4s%1s%1s%2s%4s%4s%2s%4s%2s%2s%2s%2s%2s");

        return $v;
    }

    public function getValues($nb)
    {
        $values = [];
        for ($i = 0; $i < $nb+1; $i++) {
            $trame = $this->trames[$i];
            $t = substr($trame, 0, 1);
            list($t, $o, $r, $c, $n, $v, $a, $x, $year, $month, $day, $hour, $min, $sec) =
                sscanf($trame, "%1s%4s%1s%1s%2s%4s%4s%2s%4s%2s%2s%2s%2s%2s");

            $values[] = array($c => $v);
        }
        unset($values[0]);
        return $values;
    }


    public function read($nb)
    {
        $trame = $this->trames[$nb];
        $t = substr($trame, 0, 1);

        list($t, $o, $r, $c, $n, $v, $a, $x, $year, $month, $day, $hour, $min, $sec) =
            sscanf($trame, "%1s%4s%1s%1s%2s%4s%4s%2s%4s%2s%2s%2s%2s%2s");
        echo("<br />$nb - TYPE: $t GROUP: $o REQ: $r SENSOR_TYPE: $c SENSOR_NB: $n VALUE:$v ANS: $a BLABLA: $x DATE:  $month-$day-$year, $hour:$min:$sec<br />");
    }


    public function sendTrame(string $trame)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://projets-tomcat.isep.fr:8080/appService/?ACTION=COMMAND&TEAM=G10B&TRAME=" . $trame);
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

    }

}