<?php

namespace src\Model\House;

use lib\DomHouseFramework\Model\AbstractRepository;

class HouseRepository extends AbstractRepository
{
    public function add(array $house)
    {
        $req = $this->database->prepare('INSERT INTO house_type(name,description,id_user) VALUES(?,?,?)');
        $req->execute(array($house['name'], $house['description'], $house['id_user']));
        $req->closeCursor();
    }

    public function getByUser($id)
    {
        $data = $this->database->query('SELECT * FROM house_type WHERE id_user='.$id);
        return $data;
    }

    public function getAll()
    {
        $req = $this->database->query('SELECT * FROM house_type');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM house_type WHERE id_house='.$id);

    }


}
