<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 15/04/2019
 * Time: 10:02
 */

namespace src\Model\Admin;


use lib\DomHouseFramework\Model\AbstractRepository;

class SensorTypeRepository extends AbstractRepository
{
    public function getAll()
    {
        $req = $this->database->query('SELECT * FROM composant_type ');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function add(array $sensorTypes)
    {
        $req = $this->database->prepare('INSERT INTO composant_type(name,reference,type_compo) VALUES(?,?,?)');
        $req->execute(array($sensorTypes['name'], $sensorTypes['reference'],$sensorTypes['type_compo'],));
        $req->closeCursor();
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM composant_type WHERE id_composant_type=' . $id);
    }

    public function update($id, $data)
    {
        $req = $this->database->prepare('UPDATE composant_type
                                          SET name=?,type_compo=?,reference=?,user=?
                                          WHERE id_composant_type=' . $id);
        $req->execute(array($data['name'], $data['type_compo'],$data['reference'],$data['user']));
        $req->closeCursor();
        return;
    }
    public function get($id)
    {
        $req = $this->database->query('SELECT * FROM composant_type WHERE id_composant_type=' . $id);
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data[0];
    }



}