<?php

namespace src\Model\Admin;

use lib\DomHouseFramework\Model\AbstractRepository;


/**
 * Class RoomTypeRepository : Database actions for RoomTypes
 * @package src\Model\Admin
 */
class RoomTypeRepository extends AbstractRepository
{
    public function getAll()
    {
        $req = $this->database->query('SELECT * FROM room_type');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function add(array $roomType)
    {
        $req = $this->database->prepare('INSERT INTO room_type(name,description) VALUES(?,?)');
        $req->execute(array($roomType['name'], $roomType['description']));
        $req->closeCursor();
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM room_type WHERE id_room_type=' . $id);
    }

    public function update($id, $data)
    {
        $req = $this->database->prepare('UPDATE room_type 
                                          SET name=?,description=?
                                          WHERE id_room_type=' . $id);
        $req->execute(array($data['name'], $data['description']));
        $req->closeCursor();
        return;
    }

    public function get($id)
    {
        $req = $this->database->query('SELECT * FROM room_type WHERE id_room_type=' . $id);
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data[0];
    }
}

