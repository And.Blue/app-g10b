<?php
namespace src\Model;

use lib\DomHouseFramework\Model\AbstractRepository;

class UserRepository extends AbstractRepository
{
    /**
     * Gets all users that have an activated account
     * @return array
     */
    public function getAll()
    {
        $req = $this->database->query('SELECT * FROM user WHERE is_active=1 ORDER BY create_at DESC ');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getAllEmails() {
        $req = $this->database->query('SELECT email FROM user ORDER BY create_at DESC ');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }
    /**
     * Gets all accounts that are clients
     * @return array
     */
    public function getClients()
    {
        $req = $this->database->query('SELECT * FROM user WHERE role="ROLE_USER" AND is_active=1 ORDER BY create_at DESC ');
        $data = $req->fetchAll();
        $req->closeCursor();
        return $data;
    }

    /**
     * To add a user using an array
     * @param array $user
     */
    public function add(array $user)
    {
        //a user when created is always a client...
        $req = $this->database->prepare('INSERT INTO user(name,surname,address,city,postal,email,password,role,phone, is_active) VALUES(?,?,?,?,?,?,?,?,?,0)');
        $req->execute(array($user['name'], $user['surname'], $user['address'], $user['city'], $user['postal'], $user['email'], $user['password'], 'ROLE_USER', $user['phone']));
        $req->closeCursor();
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM user WHERE id_user=' . $id);
    }

    public function update($id, $data)
    {
        $req = $this->database->prepare('UPDATE user 
                                          SET name=?,surname=?,address=?,city=?,postal=?,email=?,role=?,phone=?
                                          WHERE id_user=' . $id);
        $req->execute(array($data['name'], $data['surname'], $data['address'], $data['city'], $data['postal'], $data['email'], $data['role'], $data['phone']));
        $req->closeCursor();
    }

    public function setActivation($status, $id)
    {
        $req= $this->database->prepare('UPDATE user SET is_active=? WHERE id_user=?');
        $req->execute(array($status,$id));
        $req->closeCursor();
    }

    public function getAllUnactivated()
    {
        $req = $this->database->query('SELECT * FROM user WHERE is_active = 0 OR is_active IS NULL');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }



    public function get($id)
    {
        $req = $this->database->query('SELECT user.* FROM user WHERE id_user=' . $id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getUserCredentials($email)
    {

        $req = $this->database->prepare('SELECT * FROM user WHERE email=? limit 1');
        $req->execute(array($email));
        $data = $req->fetchAll();
        $req->closeCursor();

        return $data;
    }

}
