<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 23/05/2019
 * Time: 22:33
 */

namespace src\Model\Client;


use lib\DomHouseFramework\Model\AbstractRepository;

class HouseClientRepository extends AbstractRepository
{
    public function getAll()
    {
        $req = $this->database->query('SELECT * FROM house_room');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function add(array $house)
    {
        var_dump($house);
        $req = $this->database->prepare('INSERT INTO house_room(id_house,address,id_user,code_postal) VALUES(?,?,?,?)');
        $req->execute(array((int)$house['house-type'], $house['address'], $house['id_user'],$house['code_postal']));
        echo 'in add';
        $req->closeCursor();
    }

    public function getValue($id)
    {
        $req = $this->database->query(
            'SELECT ht.name as house_name,hr.id_house_room as house_room, hr.address as house_address, hr.code_postal as house_postal FROM house_room as hr 
            INNER JOIN house_type as ht ON hr.id_house = ht.id_house WHERE hr.id_user='.$id.'');

        $houses = $req->fetchAll(\PDO::FETCH_ASSOC);

        return $houses;
    }

    public function getIdHouseRoom($id)
    {
        $req = $this->database->query('SELECT id_house_room FROM house_room where id_house_room =' . $id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

}