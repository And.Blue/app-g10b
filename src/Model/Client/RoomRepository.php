<?php

namespace src\Model\Client;

use lib\DomHouseFramework\Model\AbstractRepository;


class RoomRepository extends AbstractRepository
{
    public function getAll(): array
    {
        $req = $this->database->query('SELECT * FROM room');
        $data = $req->fetchAll();
        $req->closeCursor();
        return $data;
    }


    public function add($userId, $roomName, $roomType, $roomIdHouse)
    {
        $req = $this->database->prepare('INSERT INTO room(name,id_room_type,id_user,id_house_room) VALUES(?,?,?,?)');
        $req->execute(array($roomName, $roomType, $userId, $roomIdHouse));
        $req->closeCursor();
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM room WHERE id_room=' . $id);
    }

    public function getByUser($id)
    {
        $req = $this->database->query('SELECT room.id_house_room as room_house ,room.id_room,room_type.name as type_name, room.name as room_name
                  FROM room INNER JOIN room_type ON room.id_room_type = room_type.id_room_type WHERE id_user=' . $id);

        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getById($id)
    {
        $req = $this->database->query('SELECT * FROM room WHERE id_room='.$id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }
    public function getId($id)
    {
        $req = $this->database->query('SELECT id_room FROM room where id_room='.$id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function update($id, $data)
    {
        $req = $this->database->prepare('UPDATE room 
                                              SET name=?  
                                              WHERE id_room=' . $id);
        $req->execute(array($data['name']));
        $req->closeCursor();
    }

//
//    public function update($id, $data)
//    {
//        $req = $this->database->prepare('UPDATE user
//                                          SET name=?,surname=?,address=?,city=?,postal=?,email=?,role=?,phone=?
//                                          WHERE id_user=' . $id);
//        $req->execute(array($data['name'], $data['surname'], $data['address'], $data['city'], $data['postal'], $data['email'], $data['role'],$data['phone']));
//        $req->closeCursor();
//
//    }


}

