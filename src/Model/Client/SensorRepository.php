<?php

namespace src\Model\Client;

use lib\DomHouseFramework\Model\AbstractRepository;


class SensorRepository extends AbstractRepository
{
    public function getAll(): array
    {
        $req = $this->database->query('SELECT * FROM composant INNER JOIN composant_type');
        $data = $req->fetchAll();
        $req->closeCursor();
        return $data;
    }


    public function add($sensorName, $sensorType, $idUser, $idRoom, $houseId)
    {
        $req = $this->database->prepare('INSERT INTO composant(nom,id_composant_type,id_user,id_room,id_house) VALUES(?,?,?,?,?)');
        $req->execute(array($sensorName, $sensorType, $idUser, $idRoom, $houseId));
        $req->closeCursor();
    }

    public function getValue($id)
    {

        $req = $this->database->query('SELECT composant_type.type_compo,composant.id_composant,composant.id_room,composant.nom as composant_nom,composant_type.type_compo as composant_type,composant.id_composant as composant_id,composant.id_room as composant_id_room
FROM composant
JOIN composant_type
ON composant_type.id_composant_type=composant.id_composant_type
 WHERE id_user=' . $id);
        $data = $req->fetchAll();
        $req->closeCursor();
        
        return $data;
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM composant WHERE id_composant=' . $id);
    }


    public function getIdHouse()
    {

        $req = $this->database->query('SELECT house_room.id_house
    FROM house_room
    JOIN composant
    ON house_room.id_user=composant.id_user');
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getIdRoom($id)
    {

        $req = $this->database->query('SELECT room.id_room
    FROM room
    JOIN composant
    ON room.id_room=' . $id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getById($id)
    {
        $req = $this->database->query('SELECT * FROM composant WHERE id_composant=' . $id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function getAllByHouse($id)
    {
        $req = $this->database->query('SELECT r.id_room,id_house, c.id_composant as id_component, 
        c.is_active as is_active_component, c.valeur as value_component, c.nom as name_component, r.name 
        FROM composant as c INNER JOIN room as r ON c.id_room = r.id_room WHERE id_house=' . $id);

        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }


    public function update($id, $data)
    {
        $req = $this->database->prepare('UPDATE composant 
                                              SET nom =?  
                                              WHERE id_composant = ' . $id);
        $req->execute(array($data['nom']));
        $req->closeCursor();
        return;
    }

    public function updateIsActive($id, $status)
    {
        $req = $this->database->prepare('UPDATE composant 
                                              SET is_active =?  
                                              WHERE id_composant = ' . $id);
        $req->execute(array($status));

        $req = $this->database->query('SELECT * FROM composant WHERE id_composant= '.$id);
        return $req->fetchAll(\PDO::FETCH_ASSOC)[0]; //fetching updated sensor (fully)


    }
}

