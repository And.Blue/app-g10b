<?php

namespace src\Model\Information;

use lib\DomHouseFramework\Model\AbstractRepository;

/**
 * This Repository handles the General Annoucements on the website
 * Class InformationRepository
 * @package src\Model\Information
 */
class InformationRepository extends AbstractRepository
{
//'SELECT room.id_room,room_type.name as type_name, room.name as room_name
//                  FROM room INNER JOIN room_type ON room.id_room_type = room_type.id_room_type WHERE id_user=' . $id);

    public function getAll()
    {
        $req = $this->database->query('SELECT announcement.id_announcement, announcement.title, announcement.text, announcement.modif_at, 
          announcement.create_at, user.surname, user.name FROM announcement INNER JOIN user ON announcement.id_user = user.id_user ORDER BY modif_at DESC ');
        $data = $req->fetchAll(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

    public function add(array $annoucement)
    {
        $req = $this->database->prepare('INSERT INTO announcement(id_user,title, text) VALUES(?,?,?)');
        $req->execute(array($annoucement['id_user'], $annoucement['title'], $annoucement['text']));
        $req->closeCursor();
    }

    public function delete($id)
    {
        $this->database->query('DELETE FROM announcement WHERE id_announcement=' . $id);
    }

    public function update($id, array $annoucement)
    {
        $req = $this->database->prepare('UPDATE announcement
                                          SET title=?,text=?,modif_at=CURRENT_TIMESTAMP
                                          WHERE id_announcement=' . $id);
        $req->execute(array($annoucement['title'], $annoucement['text']));
        $req->closeCursor();
    }

    public function get($id)
    {
        $req = $this->database->query('SELECT * FROM announcement WHERE id_announcement=' . $id);
        $data = $req->fetch(\PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $data;
    }

}

