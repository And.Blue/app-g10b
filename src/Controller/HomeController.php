<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/28/2019
 * Time: 4:17 PM
 */

namespace src\Controller;

use lib\DomHouseFramework\Controller\AbstractController;


class HomeController extends AbstractController
{
    /**
     * route: /
     */
    public function index()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] == 'ROLE_ADMIN') {
            return $this->redirectToRoute('/admin/users/all');
        }
        return $this->render('templates/pages/home.php');
    }

    public function home()
    {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            return $this->render('templates/pages/myHomeLogIn.php');
        } else {

//            return $this->render('templates/pages/myHomeLogOut.php');
            return $this->render('templates/pages/myHomeLogIn.php');

        }
    }


    public function login()
    {

        return $this->render('templates/pages/login.php');
    }

    public function register()
    {

        return $this->render('templates/pages/register.php', null);
    }

    public function info()
    {
        if (isset($_SESSION['logged']) && $_SESSION['logged']) {
            $name = $_SESSION['user']['name'];
            $surname = $_SESSION['user']['surname'];
            $address = $_SESSION['user']['address'];
            $email = $_SESSION['user']['email'];
            $message = 'is_logged';

        } else {
            $name = 'Ex : Dupont';
            $surname = 'Ex : Michel';
            $address = "Ex : 51 rue des Tilleuls";
            $email = "Ex : Dupont.Michel@gmail.com";
            $message = 'is_not_logged';
        }

        $data['name'] = $name;
        $data['surname'] = $surname;
        $data['address'] = $address;
        $data['email'] = $email;

        $this->render('templates/pages/info.php', $data, $message);
    }


    public function legal()
    {
        return $this->render('templates/pages/user/mentionsLegales.php');
    }

}
