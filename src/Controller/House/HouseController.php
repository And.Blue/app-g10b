<?php

namespace src\Controller\House;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\House\HouseRepository;

class HouseController extends AbstractController
{
    protected $houseRepo;

    /**
     * HouseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->houseRepo = new HouseRepository();
    }

    public function addHome($userId)
    {
        if ($_SESSION['role'] === 'ROLE_ADMIN' || $_SESSION['user']['id_user']) {
            if (isset($_POST['description'], $_POST['name'])) {
                if (strlen($_POST['name']) < 2) {
                    $templateData['message'] = 'Une maison doit forcément avoir un nom';
                } else {
                    $house['description'] = $_POST['description'];
                    $house['name'] = $_POST['name'];
                    $house['id_user'] = $userId;
                    $this->houseRepo->add($house);
                    return $this->redirectToRoute('/user/edit?' . $userId);
                }
            }
            $templateData['house'] = ''; //this is just so that templateData doesn't throw an error
            return $this->render('templates/Admin/pages/users/house/addHouse.php', $templateData);
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }

}
    public function delete()
    {
        if ($_SESSION['role'] === 'ROLE_ADMIN' || $_SESSION['user']['id_user']) {
            if (isset($_GET['id_house'], $_GET['id_user'])) {
                $this->houseRepo->delete($_GET['id_house']);
                return $this->redirectToRoute('/user/edit?' . $_GET['id_user']);
            }
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }

    public function getAll()
    {
        $data = $houseType = $this->houseRepo->getAll();
        echo json_encode($data);
    }


}
