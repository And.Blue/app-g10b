<?php

namespace src\Controller\Authentication;

use lib\DomHouseFramework\Forms\Form;
use lib\DomHouseFramework\Model\PosterHelper;
use lib\DomHouseFramework\View\Templator;
use src\Model\UserRepository;
use lib\DomHouseFramework\Controller\AbstractController;
use lib\DomHouseFramework\Util\ValidationHelper;

class RegisterController extends AbstractController
{
    protected $userRepo;
    protected $postManager;
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepository();
        $this->postManager = new PosterHelper();
    }

    public function cond_util(){
        return $this->render('templates/pages/user/cond_utilisations.php');
    }

    /**
     * Adds user to DataBase
     */
    public function register()
    {
        $userForm = new Form('templates/pages/user/register.php');
        $fields = array('surname', 'name', 'address', 'city', 'postal', 'email', 'password', 'password-conf', 'phone');
        $validationHelper = new ValidationHelper();
        $userRepo = new UserRepository();
        $userForm->add($fields);

        $userForm->buildView();
        if ($userForm->isComplete()) {
            $user = $userForm->getData();


            //verifying for the user terms
            if (strlen($user['password']) < 6) {
                $validationHelper->displayWarning('Le Mot-de-Passe choisi est trop court! (minimum 6 caractères)');
            } else if (!filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {
                $validationHelper->displayWarning('Veuillez rentrer un email avec un format correct! ');
            } else if (!isset($_POST['cond-util'])) {
                echo '<div class="isa_warning"><i class="fa fa-warning"></i>Les conditions d\'utilisation n\'ont pas étés validées</div>';
            } else if ($user['password'] === $user['password-conf']) {
                $usedEmails = $userRepo->getAllEmails();
                $emailOk = true;
                foreach ($usedEmails as $subarr) {
                    if($subarr['email'] === $user['email']) {
                        $emailOk = false;
                    }
                }
                if (!$emailOk) {
                    $validationHelper->displayError('Un autre utilisateur avec le même email existe déjà!');
                } else {
                    $user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
                    $this->userRepo->add($user);
                    echo '<div class="isa_success "><i class="fa fa-check"></i>' . $user['name'] . ' ' . $user['surname'] . ' a été ajouté.</div>';
                }

            } else {
                echo '<div class="isa_warning "><i class="fa fa-warning"></i>Les Mots-de-Passes ne sont pas identiques</div>';
            }
        } else if ($userForm->getSubmittedOnce()) {
            echo '<div class="isa_error "><i class="fa fa-times-circle"></i>Tous les champs n\'ont pas étés complétés...</div>';
        }
    }
}

