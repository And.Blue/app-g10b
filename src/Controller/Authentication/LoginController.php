<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 3/25/2019
 * Time: 9:29 AM
 */

namespace src\Controller\Authentication;


use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\UserRepository;
use lib\DomHouseFramework\Model\PosterHelper;
use lib\DomHouseFramework\Forms\Form;

class LoginController extends AbstractController
{
    protected $postManager;
    protected $userRepo;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepository();
        $this->postManager = new PosterHelper();

    }


    public function index()
    {
        return $this->render('templates/pages/info.php');
    }


    /**
     * User login, verifying passwords and user names.
     */
    public function login()
    {
        $userForm = new Form('templates/pages/user/login.php');
        $userForm->add('email');
        $userForm->add('password');

        $userForm->buildView();

        if ($userForm->isComplete()) {
            $pwAttempt = $userForm->getData()['password'];
            $user = $this->userRepo->getUserCredentials($userForm->getData()['email']);

            if (isset($user[0]['password']) && password_verify($pwAttempt, $user[0]['password'])) {
                $validation['status'] = 'success';

                if ($user[0]['is_active'] !== 1) {
                    echo '<div class="isa_error "><i class="fa fa-times-circle"></i>Compte n\'est pas validé, veuillez contacter admin</div>';
                } else {

                    $_SESSION['user'] = $user[0];
                    $_SESSION['logged'] = true;
                    $_SESSION['role'] = $user[0]['role'];

                    $redirectionPath =  $_SESSION['role'] === 'ROLE_ADMIN' ? 'admin/information/all' : 'announce';

                    echo '<div class="isa_success"><i class="fa fa-check"></i>Login réussi !   <em><a href="' . PATH .$redirectionPath.'">redirection</a> </em></div>';
                    echo '<meta http-equiv="refresh" content="10" > ';
                }
            } else {
                $validation['status'] = 'error';
                echo '<div class="isa_error "><i class="fa fa-times-circle"></i>Mauvais identifiant et/ou Mot-de-Passe...</div>';
            }
        }

    }

    public function logout()
    {
        $_SESSION['logged'] = true;
        session_destroy();
        return $this->redirectToRoute('/home');

    }


}

//
///**
// * Modal Login From Modal using AJAX POST
// */
//public function modalLogin()
//{
//
//    $this->postManager->add('surnameLogin');
//
//    if ($this->postManager->isComplete()) {
//        $surname = isset($_POST['surnameLogin']) ? $_POST['surnameLogin'] : 'not defined';
//        $response['user'] = $this->userRepo->getUserCredentials($surname);
//
//        if (count($response['user']) != 0) {
//            //success !
//            $response['status'] = 'log-success';
//        } else {
//            //didn't find the user
//            $response['status'] = 'Pas trouvé d\'utilisateur correspondant.';
//        }
//    }
//    else {
//        //nothing found , error
//        $response['status'] = 'erreur côté serveur...';
//    }
//
//    var_dump($response);
//
//    echo json_encode($response);
//}