<?php
/**
 * Created by IntelliJ IDEA.
 * User: narin
 * Date: 23/05/2019
 * Time: 12:13
 */
namespace src\Controller\Client;
use src\Model\Client\HouseClientRepository;
use src\Model\House\HouseRepository;

class HouseClientController extends \lib\DomHouseFramework\Controller\AbstractController
{
    protected $houseRepo;
    public function __construct()
    {
        parent::__construct();
        $this->houseRepo = new HouseClientRepository();
    }

    public function addHome()
    {
        if ($_SESSION['role'] === 'ROLE_ADMIN' || isset($_SESSION['user'])) {
            if (isset($_POST['house-type'], $_POST['address'],$_POST['code_postal'])) {
                {
                    $house['house-type'] = $_POST['house-type'];
                    $house['address'] = $_POST['address'];
                    $house['code_postal'] = $_POST['code_postal'];
                    $house['id_user'] = $_SESSION['user']['id_user'];

                    $this->houseRepo->add($house);
                    return $this->redirectToRoute('/my-house');
                }
            }


        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }
}
