<?php

namespace src\Controller\Client;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Client\RoomRepository;
use src\Model\Client\SensorRepository;
use src\Model\Client\HouseClientRepository;
use src\Model\Information\InformationRepository;
use src\Model\Integration\PasserelleInterface;

class ClientController extends AbstractController
{

    protected $infoRepo;

    public function __construct()
    {
        parent::__construct();
        $this->infoRepo = new InformationRepository();

    }

    //route : my-home
    public function myHome($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $roomRepo = new RoomRepository();
            $houseRepo = new HouseClientRepository();

            $templateData['id_house_room'] = $houseRepo->getIdHouseRoom($id);
            $templateData['rooms'] = $roomRepo->getByUser($_SESSION['user']['id_user']);

            //For the sensor part
            $sensorRepo = new SensorRepository();
            $templateData['id_room'] = $sensorRepo->getIdRoom($id);
            $templateData['id_house'] = $sensorRepo->getIdHouse();
            $_SESSION['id_specific_room'] = $id;


            $this->render('templates/pages/Client/RoomsAndSensors.php', $templateData);
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');
        }
    }

    private function getBindedComponents()
    {
        $passInterface = new PasserelleInterface('http://projets-tomcat.isep.fr:8080/appService/?ACTION=GETLOG&TEAM=G10B');
        $allValues = $passInterface->getValues(3);

        //sensor id 8 is 3
        //sensor id 23 is 7
        //sensor id 6 is 5
        foreach ($allValues as $value) {
            switch (key($value)) {
                case '3':
                    $binded[8] = $value[key($value)];
                    break;
                case '5':
                    $binded[6] = $value[key($value)];
                    break;
                case '7':
                    $binded[23] = $value[key($value)];
                    break;

            }
        }

        return $binded;
    }


    private function displayNice($input)
    {
        foreach ($input as $elem) {
            echo 'sensor key: ' . key($elem) . ' value: ' . $elem[key($elem)] . '<br>';
        }

    }


    //route: index_home
    public function myHomeSensor($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $sensorRepo = new SensorRepository();
            $templateData['id_room'] = $sensorRepo->getIdRoom($id);
            $templateData['id_house'] = $sensorRepo->getIdHouse();
            $_SESSION['id_specific_room'] = $id;
            $this->render('templates/pages/Client/getSensor.php', $templateData);

        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }
    }

    public function show()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $sensorRepo = new SensorRepository();
            $templateData['sensors'] = $sensorRepo->getValue($_SESSION['user']['id_user']);

            $this->render('templates/pages/Client/addSensor.php', $templateData);
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');
        }
    }

    public function mySensors($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $roomRepo = new RoomRepository();
            $sensorRepo = new SensorRepository();
            $_SESSION['id_room'] = $roomRepo->getId($id);
            $_SESSION['id_house'] = $sensorRepo->getIdHouse();
            $this->render('templates/pages/Client/getSensor.php');
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }

    }

    //route: my-house
    public function myHouse()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $houseRepo = new HouseClientRepository();

            $templateData['houses'] = $houseRepo->getValue($_SESSION['user']['id_user']);

            $this->render('templates/pages/Client/houses.php', $templateData);

        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');
        }
    }

    public function houseadd()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $this->render('templates/pages/Client/AddHouse.php');
        }
    }


    public function add()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $sensorRepo = new SensorRepository();
            $templateData['sensors'] = $sensorRepo->getValue($_SESSION['user']['id_user']);

            $this->render('templates/pages/Client/addSensor.php', $templateData);
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }

    }

    public function sensorEdit($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $sensorRepo = new SensorRepository();
            $templateData = $sensorRepo->getById($id);
            $this->render('templates/pages/Client/sensorEdit.php', $templateData);
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }
    }


    //route: my-home/room/edit?$id
    public function roomEdit($id)
    {

        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $roomRepo = new RoomRepository();
            $templateData = $roomRepo->getById($id);

            $this->render('templates/pages/Client/roomEdit.php', $templateData);
        } else {
            return $this->render('templates/pages/myHome.php');
        }

    }

    public function seeHomeAnnouncements()
    {
        if (isset($_SESSION['role']) && ($_SESSION['role'] === 'ROLE_USER')) {
            $annoucements = $this->infoRepo->getAll();
            return $this->render('templates/pages/Announce.php', $annoucements);
        } else {
            return $this->render('templates/pages/home.php');
        }
    }


}



