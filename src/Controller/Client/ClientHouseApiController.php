<?php

namespace src\Controller\Client;

use src\Model\Client\HouseClientRepository;
use src\Model\Client\SensorRepository;
use src\Model\House\HouseRepository;
use lib\DomHouseFramework\Controller\AbstractController;
use lib\DomHouseFramework\Model\Response;
use lib\DomHouseFramework\Util\JsonHelper;
use src\Model\Integration\PasserelleInterface;

class ClientHouseApiController extends AbstractController
{
    protected $houseRepo;


    public function __construct()
    {
        parent::__construct();
        $this->houseRepo = new HouseClientRepository();
    }


    public function getAllHomeSensors($id)
    {
        $sensorRepo = new SensorRepository();
        $allSensors = $sensorRepo->getAllByHouse($id);
        $passInterface = new PasserelleInterface('http://projets-tomcat.isep.fr:8080/appService/?ACTION=GETLOG&TEAM=G10B');
        $randomValues = $passInterface->getValues(50); //50 random values added to components
        $bindedValues = $this->getBindedComponents();

        if (sizeof($allSensors) > 1) {
            $rooms = [];
            $valueCount = 0;
            //regrouping sensors by room instead of house
            foreach ($allSensors as $sensor) {

                $rooms[$sensor['id_room']]['room'] = array(
                    'id_room' => $sensor['id_room'],
                    'id_house' => $sensor['id_house'],
                    'name' => $sensor['name']
                );


                switch ((string)$sensor['id_component']) {
                    case '8':
                        $sensor['value_component'] = $bindedValues[8];
                        break;
                    case '23':
                        $sensor['value_component'] = $bindedValues[23];
                        break;
                    case '6':
                        $sensor['value_component'] = $bindedValues[6];
                        break;
                    default:
//                        $randomValues[$valueCount]
                        $sensor['value_component'] = 'lorem';
                        $valueCount++;
                        break;

                }


                $rooms[$sensor['id_room']]['sensors'][] = $sensor;



                unset($sensor['name'], $sensor['id_room'], $sensor['id_house']);


            }
            $response = new Response(200, $rooms);
            echo $response->sendJson();
        } else {
            $response = new Response(400);
            echo $response->sendJson();
        }
    }

    private function getBindedComponents()
    {
        $passInterface = new PasserelleInterface('http://projets-tomcat.isep.fr:8080/appService/?ACTION=GETLOG&TEAM=G10B');
        $allValues = $passInterface->getValues(3);

        //sensor id 8 is 3
        //sensor id 23 is 7
        //sensor id 6 is 5
        foreach ($allValues as $value) {
            switch (key($value)) {
                case '3':
                    $binded[8] = $value[key($value)];
                    break;
                case '5':
                    $binded[6] = $value[key($value)];
                    break;
                case '7':
                    $binded[23] = $value[key($value)];
                    break;

            }
        }

        return $binded;
    }

    public function addHome()
    {
        if ($_SESSION['role'] === 'ROLE_ADMIN' || isset($_SESSION['user'])) {
            if (isset($_POST['house-type'], $_POST['address'], $_POST['code_postal'])) {
                {
                    $house['house-type'] = $_POST['house-type'];
                    $house['address'] = $_POST['address'];
                    $house['code_postal'] = $_POST['code_postal'];
                    $house['id_user'] = $_SESSION['user']['id_user'];

                    $this->houseRepo->add($house);
                    return $this->redirectToRoute('/my-house');
                }
            }

        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }

    ///api/sensors/toggle
    public function toggleActionComponent($id = null)
    {
        $sensorRepo = new SensorRepository();
        $passInterface = new PasserelleInterface("1G10B17020007BABAB220190620180924");
        //POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $postSensor = JsonHelper::convertStdToArray(json_decode(file_get_contents("php://input")));

            $updatedSensor = $sensorRepo->updateIsActive($postSensor['id_component'], $postSensor['is_active_component']);

            if ($postSensor['is_active_component'] === 1) {
                $passInterface->sendTrame("1G10B1a010001BABAC"); //activates actionneur
//                $updatedSensor['status'] = 'became true';
            } else {
                $passInterface->sendTrame("1G10B1a010000BABAC"); //desactivates actionneur
//                $updatedSensor['status'] = 'became false';

            }

            if (sizeof($updatedSensor) > 0) {
                $res = new Response(200, $updatedSensor);
                echo $res->sendJson();
                return;
            }
            $res = new Response(404);
            echo $res->sendJson();
            return;


        } else {
            if ($id === null) {
                $res = new Response(404, 'Should provide id, ex: /api/sensor/toggle?id');
                echo $res->sendJson();
                return;
            }
            $singleSensor = $sensorRepo->getById($id);

            $res = new Response(200, $singleSensor);
            echo $res->sendJson();
            return;
        }

    }


}
