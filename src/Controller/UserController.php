<?php

namespace src\Controller;

use lib\DomHouseFramework\Forms\Form;
use lib\DomHouseFramework\Model\PosterHelper;
use src\Model\House\HouseRepository;
use src\Model\UserRepository;
use src\Model\Client\HouseClientRepository;
use lib\DomHouseFramework\Controller\AbstractController;


class UserController extends AbstractController
{
    protected $userRepo;
    protected $postManager;
    protected $homeRepo;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepository();
        $this->postManager = new PosterHelper();
        $this->homeRepo = new HouseRepository();
    }

    public function index()
    {
        return $this->render('templates/pages/info.php');
    }

    /*****************************************************************************
     *  This part must only be accessed by Admins
     * ***************************************************************************
     */

    //route: /users/all
    public function showAllUsers()
    {
        // template uses it
        $data = $this->userRepo->getAll();
        return $this->render('templates/pages/users.php', $data);
    }


    //route: /users/activate
    public function accountValidation()
    {
        if (isset($_SESSION['role']) && ($_SESSION['role'] === 'ROLE_ADMIN')) {
            // template uses it
            $data = $this->userRepo->getAllUnactivated();
            return $this->render('templates/Admin/pages/users/activation.php', $data);
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }

    //route: /user/see
    //in the Admin panel, ability to see the user's information before activating/deleting his account
    public function see($id)
    {
        $user = $this->userRepo->get($id);
        return $this->render('templates/Admin/pages/users/seeAndActivate.php', $user);

    }

    //route: /activate_user
    public function toggleActivation($id)
    {
        if (isset($_SESSION['role']) && ($_SESSION['role'] === 'ROLE_ADMIN')) {
            // template uses it
            $this->userRepo->setActivation(1, $id);
            return $this->redirectToRoute('/admin/users/activate');
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }


    public function delete($id)
    {
        if (array_key_exists('type', $_GET)) {
            var_dump($_GET['type']);
            switch ($_GET['type']) {
                case 'validation':
                    $id = str_replace('&type=validation','',$id);
                    $this->userRepo->delete($id);
                    return $this->redirectToRoute('/admin/users/activate');
                case 'client':
                    $this->userRepo->delete($id);
                    return $this->redirectToRoute('/admin/users/clients');
                default:
                    echo 'passed type in URL isn\'t liked dealt with.';
            };

        }
        $this->userRepo->delete($id);
        return $this->redirectToRoute('/admin/users/all');
    }




    public function logout()
    {
        session_destroy();
        return $this->redirectToRoute('/home');

    }

    public function edit($id)
    {

        if (isset($_SESSION['role']) && ($_SESSION['role'] === 'ROLE_ADMIN' || $_SESSION['role'] === 'ROLE_USER')) {
            if ($_SESSION['role'] === 'ROLE_USER') {
                $id = $_SESSION['user']['id_user'];
            }
            $data['user_info'] = $this->userRepo->get($id);
            $data['all_homes'] = $this->homeRepo->getByUser($id);
            //checks if every information is filled in to be modified
            if (isset($_POST['surname'], $_POST['name'], $_POST['address'], $_POST['city'], $_POST['postal'], $_POST['email'], $_POST['phone'], $_POST['role'])) {
                $this->userRepo->update($id, $_POST);
                $message = 'Utilisateur bien modifié';
                return $this->render('templates/Admin/pages/userEdit.php', $data, $message);
            } else {
                $this->render('templates/Admin/pages/userEdit.php', $data);
            }
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }
    }

    //route: /user/selEdit
    public function selfEdit($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $user = $this->userRepo->get($id);
            //checks if every information is filled in to be modified
            if (isset($_POST['surname'], $_POST['name'], $_POST['address'], $_POST['city'], $_POST['postal'], $_POST['email'], $_POST['phone'], $_POST['role'])) {
                $this->userRepo->update($id, $_POST);
                $message = 'All Good';
                $_SESSION['user']['surname'] = $_POST['surname'];
                $_SESSION['user']['name'] = $_POST['name'];
                $_SESSION['user']['address'] = $_POST['address'];
                $_SESSION['user']['city'] = $_POST['city'];
                $_SESSION['user']['postal'] = $_POST['postal'];
                $_SESSION['user']['email'] = $_POST['email'];
                $_SESSION['user']['phone'] = $_POST['phone'];
                $_SESSION['user']['role'] = $_POST['role'];

                $this->render('templates/pages/user/userSelfEdit.php');

            } else {
                $this->render('templates/pages/user/userSelfEdit.php', $user);
            }
        } else {
            return $this->redirectToRoute('/illegal-credentials');
        }

    }
}


