<?php

namespace src\Controller\Messaging;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Messaging\MessageRepository;
use src\Model\Messaging\ThreadRepository;



class ThreadController extends AbstractController
{
    protected $threadRepo;
    protected $messageRepo;

    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->threadRepo = new ThreadRepository();
        $this->messageRepo = new MessageRepository();
    }

    //route: /admin/thread/all
    public function seeAllThreadsAdmin()
    {

        $this->checkRole('ROLE_ADMIN');

        $threads = $this->threadRepo->getAll();

        return $this->render('templates/Admin/pages/messaging/allThreads.php', $threads);
    }

    //route: /admin/thread/new
    public function newThreadAdmin()
    {
        $this->checkRole('ROLE_ADMIN');

        if (isset($_POST['subject'], $_POST['id_destinator'], $_POST['message'])) {
            //Setting Up thread and retrieving it's ID
            $thread['id_user_2'] = (int)$_POST['id_destinator'];
            $thread['id_user_1'] = $_SESSION['user']['id_user'];
            $thread['name'] = $_POST['subject'];
            $thread['content'] = $_POST['message'];

            $thread['id_conv'] = $this->threadRepo->add($thread)['id_conv'];
            //adding message to DB
            $this->messageRepo->add($thread);

            //redirecting to another route
            return $this->redirectToRoute('/admin/thread/all');

        }
        return $this->render('templates/Admin/pages/messaging/newThread.php');

    }

    public function viewThread($id)
    {

        $thread = $this->threadRepo->get($id);

        if (isset($_POST['new-message'])) {

            $message['content'] = $_POST['new-message'];
            $message['id_conv'] = $id;
            $message['name'] = $thread[0]['title'];
            $message['id_user_1'] = $_SESSION['user']['id_user'];
            $message['id_user_2'] = $thread[0]['id_recipient'];

            //$thread['name'],$thread['id_user_1'], $thread['id_user_2'],$thread['id_conv'],$thread['content']
            $this->messageRepo->add($message);
            unset($_POST['new-message']);
            return $this->viewThread($id);
        }

        return $this->render('templates/Admin/pages/messaging/viewThread.php', $thread);

    }

    public function deleteById($id)
    {
        $this->checkRole('ROLE_ADMIN');

        $this->threadRepo->delete($id);
        $this->messageRepo->deleteByConvId($id);

        return $this->redirectToRoute('/admin/thread/all');
    }

    //route: /admin/thread/new
    public function seeAllThreadsNew()
    {

        return $this->render('templates/Admin/pages/messaging/allThreads.php');
    }

    public function allThreads()
    {
        $this->checkRole('ROLE_USER');

        $threads = $this->threadRepo->getAllByUserId($_SESSION['user']['id_user']);
        return $this->render('templates/pages/client/messaging/all.php',$threads);

    }

    public function newThread()
    {
        $this->checkRole('ROLE_USER');

        if (isset($_POST['subject'], $_POST['id_destinator'], $_POST['message'])) {
            //Setting Up thread and retrieving it's ID
            $thread['id_user_2'] = (int)$_POST['id_destinator'];
            $thread['id_user_1'] = $_SESSION['user']['id_user'];
            $thread['name'] = $_POST['subject'];
            $thread['content'] = $_POST['message'];

            $thread['id_conv'] = $this->threadRepo->add($thread)['id_conv'];
            //adding message to DB
            $this->messageRepo->add($thread);

            //redirecting to another route
            return $this->redirectToRoute('/messages');

        }

        return $this->render('templates/pages/client/messaging/newThread.php');
    }



}
