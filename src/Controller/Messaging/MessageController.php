<?php

namespace src\Controller\Messaging;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Messaging\MessageRepository;


class MessageController extends AbstractController
{
    protected $messageRepo;

    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->messageRepo = new MessageRepository();
    }

    //route: delete_message
    public function delete($id)
    {
//        $this->checkRole('ROLE_ADMIN');
        $threadId = $this->messageRepo->getById($id)['id_conversation'];
        $this->messageRepo->deleteById($id);

        return $this->redirectToRoute('/admin/thread/view?'.$threadId);

    }
}
