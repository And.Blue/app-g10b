<?php

namespace src\Controller\Room;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Client\RoomRepository;


class RoomController extends AbstractController
{
    protected $roomRepo;

    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->roomRepo = new RoomRepository();
    }

    //route: room_add
    public function add()
    {

        $userId = $_SESSION['user']['id_user'];
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $roomName = $_POST['name'];
            $roomType = $_POST['room-type'];
            $roomIdHouse = $_SESSION['id_house_room']['id_house_room'];
            $this->roomRepo->add($userId, $roomName, $roomType, $roomIdHouse);

            return $this->redirectToRoute('/my-home?' . $roomIdHouse);
        } else {
            return $this->redirectToRoute('illegal-credentials');
        }
    }

    public function edit()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') //checks session if user is admin
        {
            //handling GET directly instead of using the frameworks default variables to make things easier but not clean though #weAreInTheRush
            $id = key($_GET);
            $houseId = $_GET['home'];
            $roomRepo = new RoomRepository();
            $myData = $roomRepo->getById($id);
            if (isset($_POST['name'])) {
                ;
                $sensorTypeUpdate['name'] = $_POST['name'];
                $roomRepo->update($id, $sensorTypeUpdate);
                return $this->redirectToRoute('/my-home?'.$houseId);
            }
            $this->render('templates/pages/Client/roomEdit.php', $myData);

        } else {
            return $this->redirectToRoute('/my-home2');
        }
    }

    public function roomDelete($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $roomRepo = new RoomRepository();
            $roomRepo->delete($id);
            $this->redirectToRoute('/my-home?'.$_SESSION['id_house_room']['id_house_room']);
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }

    }

}
