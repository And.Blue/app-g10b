<?php

namespace src\Controller\Admin;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\UserRepository;
use lib\DomHouseFramework\Model\Response;

class AdminApiUserController extends AbstractController
{
    protected $userRepo;

    /**
     * route: /users
     */
    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepository();
    }

    public function router()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                return $this->getAllUsers();
            case 'GET':
                return $this->getAllUsers();
                break;
            default:

                $res = "hehe";
                return json_decode($res);


        }
    }



    //method: 'GET'
    public function getAllUsers()
    {
        if (isset($_SESSION['role'])) //checks session if user is admin
        {
            $users = $this->userRepo->getAll();

            if (sizeof($users) > 0) {
                $response = new Response(200, $users);
                echo $response->sendJson();
            }

        } else {
            $response = new Response(401);
            echo $response->sendJson();

        }
    }


}