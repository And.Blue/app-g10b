<?php

namespace src\Controller\Admin;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\UserRepository;
use src\Model\Admin\SensorTypeRepository;

class AdminUserController extends AbstractController
{
    protected $userRepo;

    public function __construct()
    {
        parent::__construct();
        $this->userRepo = new UserRepository();;
    }


    //route:  /users/clients
    public function clients()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $clients = $this->userRepo->getClients();
            return $this->render('templates/Admin/pages/users/clients.php', $clients);
        } else {
            return $this->redirectToRoute("/illegal-credentials");
        }
    }

}