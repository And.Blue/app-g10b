<?php

namespace src\Controller\Admin\TypeManagement;


use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Admin\SensorTypeRepository;
use lib\DomHouseFramework\Model\PosterHelper;

class TypeSensorController extends AbstractController
{
    protected $sensorTypeRepo;
    protected $postManager;

    public function __construct()
    {
        parent::__construct();
        $this->sensorTypeRepo = new SensorTypeRepository();
        $this->postManager = new PosterHelper();

    }

    public function add()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            var_dump($_POST);

            if (isset($_POST['name'], $_POST['type_compo'], $_POST['reference'])) {
                $newSensorType['name'] = $_POST['name'];
                $newSensorType['type_compo'] = $_POST['type_compo'];
                $newSensorType['reference'] = $_POST['reference'];

                $this->sensorTypeRepo->add($newSensorType);

                echo '<div class="isa_success container"><i class="fa fa-check"></i>Le capteur' . $newSensorType['name'] . ' a été ajouté.</div>';
                return $this->redirectToRoute('/admin/offer/sensor-type');
            }
        } else {
            return $this->redirectToRoute('illegal-credentials');
        }

    }

    public function delete($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $this->sensorTypeRepo->delete($id);
            return $this->redirectToRoute('/admin/offer/sensor-type');
        } else {

            return $this->redirectToRoute('illegal-credentials');
        }
    }

    public function edit($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $sensorTypes = $this->sensorTypeRepo->get($id);

            if (isset($_POST['name'], $_POST['type_compo'], $_POST['reference'], $_POST['user'])) {
                $sensorTypeUpdate['name'] = $_POST['name'];
                $sensorTypeUpdate['type_compo'] = $_POST['type_compo'];
                $sensorTypeUpdate['reference'] = $_POST['reference'];
                $sensorTypeUpdate['user'] = $_POST['user'];
                $this->sensorTypeRepo->update($id, $sensorTypeUpdate);

                return $this->redirectToRoute('/admin/offer/sensor-type');
            }

            $this->render('templates/Admin/pages/sensorTypeEdit.php', $sensorTypes);
        }
        return $this->redirectToRoute('illegal-credentials');

    }
    public function getAll()
    {
        $data = $sensorTypes = $this->sensorTypeRepo->getAll();
        echo json_encode($data);
    }
    

}