<?php

namespace src\Controller\Admin\TypeManagement;

use src\Model\Admin\RoomTypeRepository;
use lib\DomHouseFramework\Model\PosterHelper;
use lib\DomHouseFramework\Controller\AbstractController;

class TypeRoomController extends AbstractController
{
    //route : room_type_add
    protected $roomTypeRepo;
    protected $postManager;

    /**
     * TypeRoomController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->roomTypeRepo = new RoomTypeRepository();
        $this->postManager = new PosterHelper();
    }

    public function add()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            if (isset($_POST['room-type'], $_POST['room-description'])) {

                $newRoomType['name'] = $_POST['room-type'];
                $newRoomType['description'] = $_POST['room-description'];

                $this->roomTypeRepo->add($newRoomType);

                echo '<div class="isa_success container"><i class="fa fa-check"></i>Le Type de pièce' . $newRoomType['name'] . ' a été ajouté.</div>';
                return $this->redirectToRoute('/admin/offer/room-type');
            }
        } else {
            return $this->redirectToRoute('illegal-credentials');
        }

    }


    //room_type_delete?$id
    public function delete($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $this->roomTypeRepo->delete($id);
            return $this->redirectToRoute('/admin/offer/room-type');
        } else {

            return $this->redirectToRoute('illegal-credentials');
        }
    }


    //route : admin/room/type_edit?$id
    public function edit($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $roomType = $this->roomTypeRepo->get($id);

            if (isset($_POST['room-type'], $_POST['room-description'])) {
                $roomTypeUpdated['name'] = $_POST['room-type'];
                $roomTypeUpdated['description'] = $_POST['room-description'];

                $this->roomTypeRepo->update($id, $roomTypeUpdated);

                return $this->redirectToRoute('/admin/offer/room-type');
            }


            $this->render('templates/Admin/pages/roomTypeEdit.php', $roomType);

        }
        return $this->redirectToRoute('illegal-credentials');

    }

    //route: get_roomtypes
    public function getAll()
    {
        $data = $roomType = $this->roomTypeRepo->getAll();
        echo json_encode($data);
    }
}
