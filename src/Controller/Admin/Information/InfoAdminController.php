<?php

namespace src\Controller\Admin\Information;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Information\InformationRepository;

class InfoAdminController extends AbstractController
{
    protected $infoRepo;

    /**
     * InfoAdminController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->infoRepo = new InformationRepository();
    }

    public function seeAnnouncements()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $annoucements = $this->infoRepo->getAll();
            return $this->render('templates/Admin/pages/information/announcements.php', $annoucements);

        } else {
            return $this->illegal();
        }
    }

    public function createAnnouncement()
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            if (isset($_POST['text'], $_POST['title'])) {
                $announcement['text'] = $_POST['text'];
                $announcement['title'] = $_POST['title'];
                $announcement['id_user'] = $_SESSION['user']['id_user'];

                $this->infoRepo->add($announcement);
                return $this->redirectToRoute('/admin/information/all');

            }
            return $this->render('templates/Admin/pages/information/create.php');
        } else {
            return $this->illegal();

        }
    }

    public function deleteAnnouncement($id)
    {
        $this->checkRole('ROLE_ADMIN');
        $this->infoRepo->delete($id);
        return $this->seeAnnouncements();

    }

    public function modifyAnnouncement($id)
    {
        $this->checkRole('ROLE_ADMIN');

        $announcement = $this->infoRepo->get($id);

        if (isset($_POST['text'], $_POST['title'])) {
            $announcement['text'] = $_POST['text'];
            $announcement['title'] = $_POST['title'];
            $this->infoRepo->update($id, $announcement);
            return $this->seeAnnouncements();

        }
        return $this->render('templates/Admin/pages/information/modify.php', $announcement);


    }
}
