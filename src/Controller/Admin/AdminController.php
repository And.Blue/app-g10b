<?php
namespace src\Controller\Admin;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Admin\RoomTypeRepository;
use src\Model\Admin\SensorTypeRepository;

class AdminController extends AbstractController
{
    public function sensorTypeManager()
    {

        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $sensorTypeRepo = new SensorTypeRepository();
            $templateData['sensorTypes'] = $sensorTypeRepo->getAll();

            return $this->render('templates/Admin/pages/sensorTypes.php',$templateData);
        } else {
            return $this->illegal();
        }
    }


    public function roomTypeManager()
    {

        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_ADMIN') //checks session if user is admin
        {
            $roomTypeRepo = new RoomTypeRepository(); //enables us to retrieve data about roomTypes

            $templateData['roomTypes'] = $roomTypeRepo->getAll(); //passed to view, could be called something else !
            return $this->render('templates/Admin/pages/roomTypes.php', $templateData);
        } else {
            return $this->illegal();
        }
    }


    //displays message saying that the user doesn't have Admin access
    public function illegal()
    {
        return  $this->render('templates/Admin/illegal-credentials.php');
    }


}