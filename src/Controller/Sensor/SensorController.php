<?php

namespace src\Controller\Sensor;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Client\SensorRepository;


class SensorController extends AbstractController
{
    protected $sensorRepo;

    /**
     * RoomController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->sensorRepo = new SensorRepository();
    }


    public function add()
    {

        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $userId = $_SESSION['user']['id_user'];
            $roomId = $_SESSION['id_room'];
            $houseId = $_SESSION['id_house'];
            $sensorName = $_POST['name'];
            $sensorType = $_POST['sensor-type'];
            var_dump($_SESSION['id_house']);
            $this->sensorRepo->add($sensorName, $sensorType, $userId, $roomId,$houseId);
            return $this->redirectToRoute('/home_sensor_add');
        } else {
            return $this->redirectToRoute('illegal-credentials');
        }

    }


    public function sensorDelete($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') {
            $sensorRepo = new SensorRepository();
            $sensorRepo->delete($id);
            $this->redirectToRoute('/home_sensor_add');
        } else {
//            return $this->render('templates/pages/myHome.php');
            return $this->redirectToRoute('/my-home2');

        }

    }

    public function edit($id)
    {
        if (isset($_SESSION['role']) && $_SESSION['role'] === 'ROLE_USER') //checks session if user is admin
        {
            $sensorRepo = new SensorRepository();
            $myData = $sensorRepo->getById($id);
            if (isset($_POST['nom'])) {
                $sensorTypeUpdate['nom'] = $_POST['nom'];
                $sensorRepo->update($id, $sensorTypeUpdate);
                return $this->redirectToRoute('/home_sensor_add');
            }
            $this->render('templates/pages/Client/sensorEdit.php',$myData);

        }
        else{
            return $this->redirectToRoute('/my-home2');
        }


    }


}