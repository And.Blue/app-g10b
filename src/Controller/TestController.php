<?php

namespace src\Controller;

use lib\DomHouseFramework\Controller\AbstractController;
use src\Model\Admin\RoomTypeRepository;
/**
 * Created by IntelliJ IDEA.
 * User: Andrew
 * Date: 2/28/2019
 * Time: 5:09 PM
 */
class TestController extends AbstractController
{
    protected $roomTypeRepo;

    public function __construct()
    {
        parent::__construct();
        $this->roomTypeRepo = new RoomTypeRepository();
    }
    public function index()
    {

        $templateData = $this->roomTypeRepo->getAll();

        return $this->render('templates/pages/test.php', $templateData);
    }

    //route: /api/home-types
    public function getHomes() {

        $data = $roomType = $this->roomTypeRepo->getAll();
        echo json_encode($data);

    }



}
